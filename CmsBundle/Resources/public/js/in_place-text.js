const InPlaceText = {
    communicate: null,
    init: function () {
        let that = this;
        this.communicate = Communicate.create(function(method, data) {});
        const inlineInjectElements = document.querySelectorAll( '[data-cms-type="text"]' );

        Array.from( inlineInjectElements ).forEach( inlineElement => {
            const config = {
                toolbar: {
                    items: [
                        'undo', 'redo',
                        '|', 'heading',
                        '|', 'bold', 'italic',
                        '|', 'link', 'insertTable',
                        '|', 'bulletedList', 'numberedList', 'outdent', 'indent'
                    ]
                },
                /*removePlugins : [
                    'Blockquote',
                    'Image',
                    'ImageCaption',
                    'ImageStyle',
                    'ImageToolbar',
                    'ImageUpload',
                    'List',
                    'EasyImage',
                    'CKFinder',
                    'CKFinderUploadAdapter'
                ]*/
            };

            InlineEditor
                .create( inlineElement, config )
                .then( editor => {
                    window.editor = editor;
                    editor.ui.focusTracker.on( 'change:isFocused', ( evt, name, isFocused ) => {
                        if ( !isFocused ) {
                            that.communicate.sendToParent(
                                'save-content',
                                {
                                    id: editor.sourceElement.getAttribute('data-cms-content-id'),
                                    content_key: editor.sourceElement.getAttribute('data-cms-content-key'),
                                    content: editor.getData()
                                });
                        }
                    } );
                } )
                .catch( error => { console.error( error ); } );
        } );
    }
};

InPlaceText.init();



