const Medias = {
    tree: null,
    optionForm: null,
    entryForm: null,
    dropzone: null,
    init: function() {
        let that = this;
        try {
            this.dropzone = new Dropzone("#dropzone", {
                'url': '#'
            });
            this.dropzone.on('processing', function(e) {
                this.options.url = $("#dropzone").data('path')+'?parent='+that.tree.getSelectedNode();
            });
            this.dropzone.on('success', function(e) {
                $('.media-list').html("");
                $('.media-options').html('');
                $('.media-entry-options').html('');
                that.loadContent(that.tree.getSelectedNode());
            });
        } catch (e) {}
        this.tree = Baseapp.Tree.create('media', 'media-tree', null, null);
        this.tree.nodeSelectionCallback = function(node) {
            $('.media-list').html("");
            $('.media-options').html('');
            $('.media-entry-options').html('');

            if (that.optionForm == null) {
                that.optionForm = Baseapp.JsForm.create('.media-options');
                that.optionForm.loadDoneCallback = function(response) {
                    $('.media-options').html(response.template);
                };
                that.optionForm.submitDoneCallback = function(response) {
                    $('.media-options').html('');
                    that.tree.refresh();
                }
            }
            that.optionForm.load($('.media-options').data('url')+"?id="+node.id);
            that.loadContent(node.id);
        };
        this.tree.controls = ['add'];
        this.tree.openAll = true;
        this.tree.run();

        that.setGuiSize();
        $(window).on('resize', function(e) {
            that.setGuiSize();
        });

        $(document).on('click', '.media-list .media-list-entry', function() {
            $('.media-entry-options').html('');
            if (that.entryForm == null) {
                that.entryForm = Baseapp.JsForm.create('.media-entry-options');
                that.entryForm.loadDoneCallback = function(response) {
                    $('.media-entry-options').html(response.template);
                };
                that.entryForm.submitDoneCallback = function(response) {
                    $('.media-entry-options').html('');
                    $('.media-list').html("");
                    that.loadContent(that.tree.getSelectedNode());
                }
            }
            that.entryForm.load($('.media-entry-options').data('url')+"?id="+$(this).data('id'));
        });
    },
    loadContent: function(parentId) {
        Baseapp.apicall(
            'cms_media_controller:loadContent',
            {
                'parent': parentId
            },
            function(response) {
                $('.media-list').html(response.template);
                Baseapp.Image.imageSize();
            }
        );
    },
    setGuiSize: function() {
        let height = window.innerHeight - $('nav.navbar').outerHeight();

        $('.media-tree-wrapper').height(height/2);
        let mediaHeight = $('.media-tree-wrapper').height() - $('.media-tree-wrapper .tree-controls').height();
        $('.media-tree-wrapper .tree-outer-wrapper').outerHeight(mediaHeight);
        $('.media-tree-wrapper .tree-outer-wrapper').width($('.media-tree-wrapper').width());

        $('.media-options').outerHeight(height/2);
        $('.media-entry-options').outerHeight(height);

        $('.media-list').outerHeight(height - ($('.media-upload-field').outerHeight() + 20));

        Baseapp.Image.imageSize();
    },
};

Medias.init();