const Editor = {
    structureTree: null,
    contentTree: null,
    structureOptionForm: null,
    contentOptionForm: null,
    communicate: null,
    run: function() {
        let that = this;
        that.gui(that);
        that.communicate(that);
    },
    communicate: function(that) {
        that.communicate = Communicate.create(function(method, data) {
            if (method === 'add') {
                Baseapp.apicall(
                    'baseapp_tree_controller:nodes',
                    {'name': 'content', 'selected': data},
                    function (response) {
                        Baseapp.Selection.show(
                            response.title,
                            response.template,
                            function (element) {
                                Baseapp.apicall(
                                    'baseapp_tree_controller:addNode',
                                    {
                                        'name': 'content',
                                        'node_name': $(element).data('type'),
                                        'parent_id': data.parent,
                                        'version': data.version,
                                        'foreignId': data.foreignId,
                                        'sorting': data.sorting
                                    },
                                    function (response) {
                                        if (response.success === true) {
                                            that.contentTree.refresh();
                                        }
                                        that.contentTree.addNodeCallback();
                                    });
                            }
                        );
                    }
                );
            }
            if (method === 'save-content') {
                Baseapp.apicall(
                    'cms_content_edit_controller:saveContent',
                    data,
                    function (response) {});
            }
            if (method === 'select-element') {
                if (that.contentTree != null) {
                    that.contentTree.selectNode(data.id);
                }
            }

        }, document.querySelector('iframe'));
    },
    gui: function(that) {
        $(document).on('change', '#versions select#version-selector', function() {
            that.loadNewContent();
        });
        $(document).on('click', '#version-actions a.delete', function() {
            let sourceId = $(this).data('sourceid');
            Baseapp.Modal.show($(this).data('title'), $(this).data('message'), $(this).data('button'), function() {
                Baseapp.apicall(
                    'cms_content_edit_controller:deleteVersion',
                    {
                        'id': sourceId
                    },
                    function(response) {
                        that.structureTree.refresh();
                    }
                );
            });
        });

        $(document).on('click', '#version-actions a.createdraft', function() {
            let sourceId = $(this).data('sourceid');
            Baseapp.Modal.show($(this).data('title'), $(this).data('message'), $(this).data('button'), function() {
                Baseapp.apicall(
                    'cms_content_edit_controller:createDraft',
                    {
                        'id': sourceId
                    },
                    function(response) {
                        that.structureTree.refresh();
                    }
                );
            });
        });

        $(document).on('click', '#version-actions a.publish', function() {
            let sourceId = $(this).data('sourceid');
            Baseapp.Modal.show($(this).data('title'), $(this).data('message'), $(this).data('button'), function() {
                Baseapp.apicall(
                    'cms_content_edit_controller:setPublish',
                    {
                        'id': sourceId
                    },
                    function(response) {
                        Cms.cacheClear();
                        that.structureTree.refresh();
                    }
                );
            });
        });

        this.structureTree = Baseapp.Tree.create('structure', 'structure-tree', null, null);
        this.structureTree.moveSuccessCallback = function(){Cms.cacheClear()};
        this.structureTree.removeNodeCallback = function() {Cms.cacheClear()};
        this.structureTree.addNodeCallback = function() {Cms.cacheClear()};
        this.structureTree.refreshCallback = function() {
            if (that.contentTree != null) {
                that.contentTree.hide();
            }
            $('.structure-options').html('');
            $('.content-options').html('');
            $('#versions').html('');
            $('#version-actions').html('');
            $('iframe#content').hide();
            $('#viewports').hide();
            document.querySelector('iframe#content').src = 'about:blank';
        }
        this.structureTree.nodeSelectionCallback = function(node) {
            Baseapp.apicall(
                'cms_content_edit_controller:versions',
                {
                    'id': node.id
                },
                function(response) {
                    $('#versions').html(response.template);
                    that.loadNewContent();
                }
            );
            if (that.contentTree != null) {
                that.contentTree.hide();
            }
            $('.structure-options').html('');
            $('.content-options').html('');
            $('#versions').html('');
            $('#version-actions').html('');
            $('iframe#content').hide();
            $('#viewports').hide();
            document.querySelector('iframe#content').src = 'about:blank';
            /*if (that.structureOptionForm == null) {
                that.structureOptionForm = Baseapp.JsForm.create('.structure-options');
                that.structureOptionForm.loadDoneCallback = function(response) {
                    $('.structure-options').html(response.template);
                };
                that.structureOptionForm.submitDoneCallback = function(response) {
                    that.structureTree.refresh();
                    Cms.cacheClear();
                }
            }
            that.structureOptionForm.load($('.structure-options').data('url')+"?id="+node.id);*/
        };
        that.structureTree.optionsForm.submitDoneCallback = function(response) {
            that.structureTree.refresh();
            Cms.cacheClear();
        }
        that.structureTree.run();

        that.setGuiSize();
        $(window).on('resize', function(e) {
          that.setGuiSize();
        });

        $(document).on('click','#version-actions a.showpreview', function(e) {
            e.preventDefault();
            let options = JSON.parse($('select#version-selector option').filter(':selected').val());
            window.open($(this).attr('href')+"?id="+options.structure_id+"&version="+options.content_version);
        });
        $(document).on('change','select#viewports-selector', function() {
            let type = $(this).val();
            if (type==='full') {
                $('iframe#content').css('max-width','100%');
            } else if (type==='tablet') {
                $('iframe#content').css('max-width','800px');
            } else if (type==='mobile') {
                $('iframe#content').css('max-width','480px');
            }
        });

    },
    setGuiSize: function() {
        let height = window.innerHeight - $('nav.navbar').outerHeight();
        $('#iframe-wrapper').height(height - $('#content-actions').height()-20);

        $('.structure-tree-wrapper').height(height/2);
        $('.content-tree-wrapper').height(height/2);
        $('.structure-options').outerHeight((height/2) - 2);
        $('.content-options').outerHeight((height/2) - 2);

        let structureHeight = $('.structure-tree-wrapper').height() - $('.structure-tree-wrapper .tree-controls').height();
        $('.structure-tree-wrapper .tree-outer-wrapper').height(structureHeight - 2);
        $('.structure-tree-wrapper .tree-outer-wrapper').width($('.structure-tree-wrapper').width());

        let contentHeight = $('.content-tree-wrapper').height() - $('.content-tree-wrapper .tree-controls').height();
        $('.content-tree-wrapper .tree-outer-wrapper').height(contentHeight - 2);
        $('.content-tree-wrapper .tree-outer-wrapper').width($('.content-tree-wrapper').width());

        Baseapp.Image.imageSize();
    },
    loadNewContent: function() {
        $('#viewports').hide();
        let that = this;
        if (this.contentTree != null) {
            this.contentTree.hide();
        }
        $('iframe#content').hide();
        document.querySelector('iframe#content').src = 'about:blank';
        try {
            let options = JSON.parse($('select#version-selector option').filter(':selected').val());

            Baseapp.apicall(
                'cms_content_edit_controller:versionActions',
                options,
                function(response) {
                    $('#version-actions').html(response.template);
                }
            );

            if (this.contentTree == null) {
                this.contentTree = Baseapp.Tree.create(
                    'content',
                    'content-tree',
                    options.content_version,
                    options.structure_id,
                );
                this.contentTree.nodeSelectionCallback = function(node) {
                    if (!that.contentTree.getSelectedNode()) {
                        return;
                    }

                    //console.log(node);
                    //jump in content edit window to the anchor but prevent an content-tree select infinite loop!
                };
                this.contentTree.optionsForm.submitDoneCallback = function(response) {
                    $('iframe#content').hide();
                    document.querySelector('iframe#content').src = 'about:blank';
                    $('#version-actions').html('');
                    that.loadNewContent();
                }
                this.contentTree.addNodeCallback = function() {that.loadNewContent()};
                this.contentTree.removeNodeCallback = function() {that.loadNewContent()};
                this.contentTree.moveSuccessCallback = function() {that.loadNewContent()};
                this.contentTree.copySuccessCallback = function() {that.loadNewContent()};
                this.contentTree.openAll = true;
                this.contentTree.run();
            } else {
                this.contentTree.version = options.content_version;
                this.contentTree.foreignId = options.structure_id;
                this.contentTree.refresh();
            }
            $('iframe#content').show();
            $('iframe#content').attr('name', Math.random());
            $('iframe#content').attr('src', options.link);
            $('#viewports').show();

        } catch (e) {
        }
    }
};

Editor.run();