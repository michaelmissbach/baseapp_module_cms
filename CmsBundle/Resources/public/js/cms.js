const Cms = {

    run: function() {
        $(document).on('click', 'a#cache-clear-action', function(e) {
            e.preventDefault();
            Cms.cacheClear();
        });
    },
    cacheClear: function() {
        Baseapp.apicall(
            'cms_content_edit_controller:cacheClear',
            {},
            function(response) {}
        );
    }
};

Cms.run();