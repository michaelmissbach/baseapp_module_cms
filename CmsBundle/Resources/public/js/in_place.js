const In_place = {
    communicate: null,
    run: function() {
        let that = this;
        this.communicate = Communicate.create(function(method, data) {});
        this.eventElements();
        this.formatElements();
        setInterval(function(){that.formatElements()},1000);
    },
    eventElements: function() {
        let that = this;
        let actions = document.querySelectorAll('[data-cms-edit]');
        for (var i = 0; i < actions.length; i++) {
            actions[i].addEventListener('click', function(){
                that.communicate.sendToParent('select-element', {id: this.getAttribute('data-cms-content-id')});
            });
        }
        let inserts = document.querySelectorAll('[data-cms-insert]');
        for (var i = 0; i < inserts.length; i++) {
            inserts[i].addEventListener('mouseenter', function(){
                this.setAttribute('data-cms-hover','true');
                this.insertAdjacentHTML("afterend", '<div data-cms-insert-current-line="true"></div>');
                this.parentElement.setAttribute('data-cms-insert-parent','true');
            });
            inserts[i].addEventListener('mouseleave', function(){
                this.removeAttribute('data-cms-hover');
                document.querySelectorAll("[data-cms-insert-current-line]").forEach(e => e.remove());
                this.parentElement.removeAttribute('data-cms-insert-parent','true');
            });
            inserts[i].addEventListener('click', function(){
                let data = {
                    'parent': this.getAttribute('data-cms-content-parent'),
                    'sorting': this.getAttribute('data-cms-content-sorting'),
                    'version': this.getAttribute('data-cms-content-version'),
                    'foreignId': this.getAttribute('data-cms-content-foreignid'),
                };
                that.communicate.sendToParent('add', data);
            });
        }
    },
    formatElements: function() {
        let inserts = document.querySelectorAll('[data-cms-insert]');
        for (var i = 0; i < inserts.length; i++) {
            inserts[i].style.top = null;
            let parent = inserts[i].parentElement;
            let rect = parent.getBoundingClientRect();
            let middle = ((rect.right - rect.left) / 2) + rect.left;
            inserts[i].style.left = (pageXOffset + middle) + 'px';
            rect = inserts[i].getBoundingClientRect();
            inserts[i].style.top = pageYOffset + (rect.top - 10) + 'px';
        }
    }
};

In_place.run();