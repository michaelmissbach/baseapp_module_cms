const EditorJavascript = {
    init: function () {
        $(document).on(Baseapp.Modal.eventNames.loaded, function() {
            let form = $('#BaseappModal form');
            if (form) {
                if ($(form).attr('data-treename') === 'content' && $(form).attr('data-optiontype') === 'javascript') {
                    let textarea = document.getElementById('form_content');
                    let editor = CodeMirror.fromTextArea(textarea, {
                        lineNumbers: true,
                        mode:  "javascript",
                        content: textarea.value
                    });
                    console.log($(textarea));
                    editor.on('blur', function() {
                        textarea.value = editor.getValue();
                    });
                }
            }
        });
    }
};
EditorJavascript.init();
