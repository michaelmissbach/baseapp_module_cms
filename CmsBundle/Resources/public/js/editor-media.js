const EditorMedia = {
    tree: null,
    communicate: null,
    init: function () {
        let that = this;
        this.communicate = Communicate.create(function(method, data) {
            if (method === 'image.select') {
                that.loadSelector(that, data.id, data.type);
            }
        });
        $(document).on('click', '.option-image-selection', function(e) {
            that.loadSelector(that, $(this).attr('data-media-id'), $(this).attr('data-media-type'));

        });
    },
    loadSelector: function(that, id, type) {
        console.log(id);
        console.log(type);
        Baseapp.apicall('cms_media_controller:loadSelector',
            {},
            function(response) {
                Baseapp.Selection.show(response.title, response.template, function(e) {
                    Baseapp.apicall(
                        'cms_media_controller:saveContent',
                        {content_id: id, media_id: $(e).data('id')},
                        function(response) {
                            Editor.loadNewContent();
                            Baseapp.Modal.hide();
                        });
                }, {width: 1000, showOk: true});
                that.tree = Baseapp.Tree.create('media', 'media-tree', null, null);
                that.tree.nodeSelectionCallback = function(node) {
                    $('.media-selection-list').html("");
                    that.loadContent(node.id, type);
                };
                that.tree.controls = [];
                that.tree.type = type;
                that.tree.openAll = true;
                that.tree.run();
            }
        );
    },
    loadContent: function(parentId, type) {
        Baseapp.apicall(
            'cms_media_controller:loadSelections',
            {
                'parent': parentId,
                'type': type
            },
            function(response) {
                $('.media-selector-list').html(response.template);
                Baseapp.Image.imageSize();
            }
        );
    }
};

EditorMedia.init();



