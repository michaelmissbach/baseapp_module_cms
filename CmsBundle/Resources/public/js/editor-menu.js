const EditorMenu = {
    tree: null,
    init: function () {
        let that = this;
        $(document).on('focus','#form_menu_entry_id', function() {
            Baseapp.apicall(
                'cms_menu_controller:loadSelector',
                {},
                function(response) {
                    Baseapp.Selection.show(response.title, response.template, function(e) {
                        $('#form_menu_entry_id').val($(e).attr('data-type'));
                    }, {width: 500, showOk: false});
                    that.tree = Baseapp.Tree.create('structure', 'structure-overlay-tree', null, null);
                    that.tree.nodeSelectionCallback = function(node) {
                        $('#BaseappSelection a.selection-ok').attr('data-type', node.id);
                        Baseapp.Selection.showOk();
                    };
                    that.tree.controls = [];
                    that.tree.openAll = true;
                    that.tree.run();
                }
            );
        });
    },
};

EditorMenu.init();



