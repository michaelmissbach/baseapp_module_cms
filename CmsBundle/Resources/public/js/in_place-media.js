const InPlaceMedia = {
    communicate: null,
    init: function () {
        let that = this;
        this.communicate = Communicate.create(function(method, data) {});
        let inlineInjectElements = document.querySelectorAll( '[data-cms-media="true"]' )
        Array.from( inlineInjectElements ).forEach( inlineElement => {
            inlineElement.addEventListener('click', function(){
                that.communicate.sendToParent(
                    'image.select',
                    {
                        id: inlineElement.getAttribute('data-cms-content-id'),
                        type: inlineElement.getAttribute('data-cms-type')
                    });
            });
        });
    }
};

InPlaceMedia.init();



