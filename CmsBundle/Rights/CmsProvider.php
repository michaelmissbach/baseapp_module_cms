<?php

namespace Shape\CmsBundle\Rights;

use BaseApp\BaseappBundle\Interfaces\IRightsProvider;

class CmsProvider implements IRightsProvider
{
    public function getRights(): array
    {
        return [
            'cms_structure',
            'cms_media',
            'cms_structure_edit',
            'cms_structure_preview',
            'baseapp_tree_load_structure',
            'baseapp_tree_load_content',
            'baseapp_tree_massiveload_content',
            'baseapp_tree_massiveload_structure',
            'baseapp_tree_edit_options_structure',
            'baseapp_tree_edit_options_content',
            'baseapp_tree_nodes_content',
            'baseapp_tree_nodes_structure',
            'baseapp_tree_add_node_content',
            'baseapp_tree_add_node_structure',
            'baseapp_tree_remove_node_content',
            'baseapp_tree_remove_node_structure',
            'baseapp_tree_copy_node_content',
            'baseapp_tree_copy_node_structure',
            'baseapp_tree_move_node_content',
            'baseapp_tree_move_node_structure',
            'baseapp_tree_add_node_media',
            'baseapp_tree_edit_options_media',
            'cms_content_save',
            'cms_content_delete_version',
            'cms_content_create_draft',
            'cms_content_set_publish',
            'cms_content_versions',
            'cms_content_version_actions',
            'cms_cache_clear',
            'cms_media_load_content',
            'cms_media_save_content',
            'cms_media_load_selections',
            'cms_media_upload',
            'cms_tree_nodes_massive_load_media',
            'cms_menu_load_selector'

        ];
    }
}