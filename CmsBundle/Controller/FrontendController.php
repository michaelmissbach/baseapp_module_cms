<?php

namespace Shape\CmsBundle\Controller;

use Shape\CmsBundle\Cache\ContentCache;
use Shape\CmsBundle\Constant\ContentMode;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Event\Rendering\ErrorPage;
use Shape\CmsBundle\Service\MediaService;
use Shape\CmsBundle\Service\Renderer;
use Shape\CmsBundle\Service\Router;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FrontendController extends AbstractController
{
    public function __construct(
        protected EntityManagerInterface $doctrine,
        protected Router $cmsRouter,
        protected Renderer $renderer,
        protected MediaService $mediaService,
        protected ContentCache $cache,
        protected EventDispatcherInterface $dispatcher,
        protected TranslatorInterface $translator
    ) {}

    public function index(Request $request, string $path)
    {
        $errorType = null;
        try {
            $matchedRoute = $this->cmsRouter->match($request, $path);

            if ($page = $matchedRoute->getMatchedPage()) {
                if (!$page->getOption(OptionKeys::HIDDEN, false)) {
                    if ($content = $this->doctrine->getRepository(Content::class)->findOneBy(['foreignId' => $page->getId(), 'parent' => null, 'mode' => ContentMode::PUBLISH])) {

                        $renderedContent = $this->renderer->renderPage($page, $content);
                        if ($this->renderer->isSiteCacheable()) {
                            $this->cache->save($request, $path, $renderedContent);
                        }
                        return new Response($renderedContent);
                    }
                }
            }

            if ($page = $matchedRoute->getRedirectToPage()) {
                return new RedirectResponse(
                    sprintf('%s://%s', $request->getScheme(), $page->getBuiltUrl()),
                    307
                );
            }

            if ($response = $this->mediaService->handleCacheElement($path)) {
                return $response;
            }

            $errorType = '404';

        } catch (\Exception $e) {
            $errorType = '500';
        }

        if ($errorType) {
            if($matchedErrorPage = $this->cmsRouter->matchErrorPage($request, (string)$errorType)) {
                if ($content = $this->doctrine->getRepository(Content::class)->findOneBy(['foreignId' => $matchedErrorPage->getId(), 'parent' => null, 'mode' => ContentMode::PUBLISH])) {
                    $renderedContent = $this->renderer->renderPage($matchedErrorPage, $content);
                    return new Response($renderedContent);
                }
            }
        }

        $errorPage = ErrorPage::create(
            $this->translator->trans('baseapp.cms.template.error.somethingwentwrong')
        );
        $this->dispatcher->dispatch($errorPage, ErrorPage::NAME);

        return new Response($errorPage->getRenderedPage());
    }
}
