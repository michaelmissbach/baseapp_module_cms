<?php

namespace Shape\CmsBundle\Controller;

use BaseApp\BaseappBundle\Interfaces\IJsonApiCallable;
use BaseApp\BaseappBundle\Service\TreeService;
use BaseApp\BaseappBundle\Service\UserService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\ContentMode;
use BaseApp\BaseappBundle\Constant\NotifyType;
use Shape\CmsBundle\Constant\RendererMode;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Event\Cache\Clear;
use Shape\CmsBundle\Service\Renderer;
use Shape\CmsBundle\Service\Router;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EditController extends AbstractController implements IJsonApiCallable
{
    public function __construct(
        protected EntityManagerInterface $doctrine,
        protected RouterInterface $router,
        protected TreeContainer $treeContainer,
        protected TreeService $treeService,
        protected Renderer $renderer,
        protected Router $cmsRouter,
        protected TranslatorInterface $translator,
        protected EventDispatcherInterface $dispatcher)
    {
    }

    public function versions(array $params): array
    {
        $entities = $this->doctrine->getRepository(Content::class)->findBy(['parent'=>null,'foreignId'=>$params['id']]);

        $versions = [];
        /** @var Content $entity */
        foreach ($entities as $entity) {
            $options = [
                'link' => $this->router->generate('cms_edit_page', ['id' => $params['id'], 'version' => $entity->getVersion()]),
                'structure_id' => $params['id'],
                'content_id' => $entity->getId(),
                'content_version' => $entity->getVersion(),
                'mode' => $entity->getMode()
            ];
            $versions[] = [
                'version' => $entity->getVersion(),
                'mode' => $entity->getMode(),
                'options' => json_encode($options)
            ];
        }

        return [
            'template' => $this->renderView('@Cms/edit/versions.html.twig', ['versions' => $versions])
        ];
    }

    public function versionActions(array $params): array
    {
        $entities = $this->doctrine->getRepository(Content::class)->findBy(['parent'=>null,'foreignId'=>$params['structure_id']]);
        $currentVersion = $this->doctrine->getRepository(Content::class)->find($params['content_id']);

        $showDeleteButton = false;
        $showPreviewButton = false;
        $createDraftButton = false;
        $setPublishButton = false;

        if (count($entities) > 1 && (
            $currentVersion->getMode() === ContentMode::DRAFT || $currentVersion->getMode() === ContentMode::OLD)
        ) {
            $showDeleteButton = true;
        }
        if ($currentVersion->getMode() === ContentMode::OLD || $currentVersion->getMode() === ContentMode::PUBLISH) {
            $draftFound=false;
            foreach ($entities as $entity) {
                if ($entity->getMode() === ContentMode::DRAFT) {
                    $draftFound = true;
                    break;
                }
            }
            if (!$draftFound) {
                $createDraftButton = true;
            }
        }
        if ($currentVersion->getMode() === ContentMode::DRAFT) {
            $setPublishButton = true;
        }
        if ($currentVersion->getMode() === ContentMode::DRAFT || $currentVersion->getMode() === ContentMode::OLD ) {
            $showPreviewButton = true;
        }


        return [
            'template' => $this->renderView('@Cms/edit/versionactions.html.twig',
                [
                    'showDeleteButton' => $showDeleteButton,
                    'createDraftButton' => $createDraftButton,
                    'setPublishButton' => $setPublishButton,
                    'showPreviewButton' => $showPreviewButton,
                    'currentId' => $params['content_id']
                ]
            )
        ];
    }

    public function deleteVersion(array $params):array
    {
        $this->treeService->removeNode('content', $params['id'], true, true, true);

        return [
            'notify' => [
                'message' => $this->translator->trans('baseapp.cms.content.version.delete.success'),
                'type' => NotifyType::SUCCESS
            ]
        ];
    }

    public function createDraft(array $params):array
    {
        $source = $this->doctrine->getRepository(Content::class)->find((int)$params['id']);
        $lastPublishedVersion = $this->doctrine->getRepository(Content::class)
            ->findOneBy(['mode'=>ContentMode::PUBLISH, 'foreignId'=>$source->getForeignId()]);
        $target = $this->treeService->copyTreeElement($source, $lastPublishedVersion->getVersion() + 1);
        $target->setMode(ContentMode::DRAFT);
        $this->doctrine->flush();

        return [
            'notify' => [
                'message' => $this->translator->trans('baseapp.cms.content.version.createdraft.success'),
                'type' => NotifyType::SUCCESS
            ]
        ];
    }

    public function setPublish(array $params):array
    {
        $entity = $this->doctrine->getRepository(Content::class)->find((int)$params['id']);

        $this->doctrine->getConnection()
            ->executeQuery(sprintf("UPDATE cms_content c SET c.mode = '%s' WHERE c.foreign_id = %s AND (c.mode = '%s' or c.mode = '%s');", ContentMode::OLD, $entity->getForeignId(), ContentMode::PUBLISH, ContentMode::DRAFT));

        $entity->setMode(ContentMode::PUBLISH);
        $this->doctrine->persist($entity);
        $this->doctrine->flush();

        return [
            'notify' => [
                'message' => $this->translator->trans('baseapp.cms.content.version.publish.success'),
                'type' => NotifyType::SUCCESS
            ]
        ];
    }

    public function cacheClear(array $params): array
    {
        $clear = Clear::create();
        $this->dispatcher->dispatch($clear, Clear::NAME);

        return [
            'notify' => [
                'message' => implode(PHP_EOL, $clear->getMessages()),
                'type' => NotifyType::SUCCESS
            ]
        ];
    }

    public function edit(Request $request)
    {
        $id = (int)$request->get('id', 0);
        $version = (int)$request->get('version', 0);

        $page = $this->doctrine->getRepository(Structure::class)->find($id);
        $content = $this->doctrine->getRepository(Content::class)->findOneBy(['foreignId' => $id, 'parent' => null, 'version' => $version]);
        if  ($content->getMode() === ContentMode::DRAFT) {
            $this->renderer->setMode(RendererMode::EDIT);
        }
        return new Response($this->renderer->renderPage($page, $content));
    }

    public function preview(Request $request)
    {
        $id = (int)$request->get('id', 0);
        $version = (int)$request->get('version', 0);
        $page = $this->doctrine->getRepository(Structure::class)->find($id);
        $content = $this->doctrine->getRepository(Content::class)->findOneBy(['foreignId' => $id, 'parent' => null, 'version' => $version]);
        return new Response($this->renderer->renderPage($page, $content));
    }

    public function saveContent(array $params): array
    {
        $entity = $this->doctrine->getRepository(Content::class)->find((int)$params['id']);
        $content = $entity->getContent();
        $noChange = ($content[$params['content_key']]??'') === $params['content'];
        if (!$noChange) {
            $content[$params['content_key']] = $params['content'];
            $entity->setContent($content);
            $this->doctrine->persist($entity);
            $this->doctrine->flush();
            return [
                'notify' => [
                    'message' => $this->translator->trans('baseapp.cms.content.message.saved'),
                    'type' => NotifyType::SUCCESS
                ]
            ];
        }
        //else silence:
        return [];
    }

    public function publishIndicator(array $params): array
    {
        $entities = $this->doctrine->getRepository(Content::class)->findBy(['parent'=>null,'mode'=>ContentMode::DRAFT]);
        return ['count' => count($entities)];
    }

    /**
     * @param string $method
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function isJsonApiAllowed(string $method, array $params): bool
    {
        if ($method === 'cacheClear') {
            return UserService::$instance->isAllowed('cms_cache_clear');
        }
        if ($method === 'saveContent') {
            return UserService::$instance->isAllowed('cms_content_save');
        }
        if ($method === 'deleteVersion') {
            return UserService::$instance->isAllowed('cms_content_delete_version');
        }
        if ($method === 'createDraft') {
            return UserService::$instance->isAllowed('cms_content_create_draft');
        }
        if ($method === 'setPublish') {
            return UserService::$instance->isAllowed('cms_content_set_publish');
        }
        if ($method === 'versions') {
            return UserService::$instance->isAllowed('cms_content_versions');
        }
        if ($method === 'versionActions') {
            return UserService::$instance->isAllowed('cms_content_version_actions');
        }
        if ($method === 'publishIndicator') {
            return UserService::$instance->isAllowed('cms_content_set_publish');
        }
        return false;
    }
}