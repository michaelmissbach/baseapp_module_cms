<?php

namespace Shape\CmsBundle\Controller;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Constant\NotifyType;
use BaseApp\BaseappBundle\Event\Tree\TypeFilter;
use BaseApp\BaseappBundle\Interfaces\IJsonApiCallable;
use BaseApp\BaseappBundle\Service\UserService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Media;
use Shape\CmsBundle\Interfaces\INodeRenderable;
use Shape\CmsBundle\Service\MediaService;
use Shape\CmsBundle\Service\Renderer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Undocumented class
 */
class MediaController extends AbstractController implements IJsonApiCallable
{
    public function __construct(protected MediaService $mediaService,
                                protected EntityManagerInterface $doctrine,
                                protected TreeContainer $treeContainer,
                                protected Environment $twig,
                                protected TranslatorInterface $translator,
                                protected EventDispatcherInterface $dispatcher,
                                protected Renderer $renderer)
    {}

    public function index()
    {
        $guiBuilder = GuiBuilder::$instance;
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/cms/js/dropzone.js'));
        $guiBuilder->getFooterStyleSheets()->add(InternStyleSheet::create()->setSource('/bundles/cms/css/dropzone.css'));
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/cms/js/media.js'));
        $guiBuilder->getFooterStyleSheets()->add(InternStyleSheet::create()->setSource('/bundles/cms/css/media.css'));

        return $this->render('@Cms/media/index.html.twig',[]);
    }

    public function loadContent(array $params): array
    {
        $entities = $this->doctrine->getRepository(Media::class)
            ->findBy(['parent'=>$params['parent']]);

        $list = [];
        foreach ($entities as $entity) {
            try {
                $node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('media', $entity);
                $tmp = [
                    'id' => $entity->getId(),
                    'node' => $node,
                    'template' => ''
                ];
                if ($node instanceof INodeRenderable) {
                    $tmp['template'] = $this->renderer->renderNode($node);
                }
                $list[] = $tmp;
            } catch (\Exception $e) {}
        }

        return ['template' => $this->renderView('@Cms/media/list.html.twig', ['list' => $list])];
    }

    public function upload(Request $request)
    {
        foreach ($request->files->all() as $uploadedFile) {
            $this->mediaService->upload((int)$request->get('parent'), $uploadedFile);
        }
        return new JsonResponse(['success'=>true]);
    }

    public function loadSelector(array $params): array
    {
        return [
            'title' => $this->translator->trans('baseapp.cms.media.title.loadSelector'),
            'template' => $this->renderView('@Cms/media/selector.html.twig', [])
        ];
    }

    public function loadSelections(array $params): array
    {
        $type = strlen($params['type']) ? $params['type'] : null;
        $entities = $this->doctrine->getRepository(Media::class)
            ->findBy(['parent'=>$params['parent']]);

        $list = [];
        foreach ($entities as $entity) {
            try {
                $node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('media', $entity);
                if (!is_null($type)) {
                    $typeFilterEvent = TypeFilter::create('media', $node, $type);
                    $this->dispatcher->dispatch($typeFilterEvent, TypeFilter::NAME);
                    if (!$typeFilterEvent->isAllowed()) {
                        continue;
                    }
                }
                $tmp = [
                    'id' => $entity->getId(),
                    'node' => $node,
                    'template' => ''
                ];
                if ($node instanceof INodeRenderable) {
                    $tmp['template'] = $this->renderer->renderNode($node);
                }
                $list[] = $tmp;
            } catch (\Exception $e) {}
        }

        return ['template' => $this->renderView('@Cms/media/selection.html.twig', ['list' => $list])];
    }

    public function saveContent(array $params): array
    {
        $entity = $this->doctrine->getRepository(Content::class)->find((int)$params['content_id']);
        $entity->addOption(OptionKeys::MEDIA_ID, $params['media_id']);
        $this->doctrine->persist($entity);
        $this->doctrine->flush();
        return [
            'notify' => [
                'message' => $this->translator->trans('baseapp.cms.content.message.saved'),
                'type' => NotifyType::SUCCESS
            ]
        ];
    }

    /**
     * @param string $method
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function isJsonApiAllowed(string $method, array $params): bool
    {
        if ($method === 'loadSelector') {
            return UserService::$instance->isAllowed('cms_media_load_selections');
        }
        if ($method === 'saveContent') {
            return UserService::$instance->isAllowed('cms_media_save_content');
        }
        if ($method === 'loadContent') {
            return UserService::$instance->isAllowed('cms_media_load_content');
        }
        if ($method === 'loadSelections') {
            return UserService::$instance->isAllowed('cms_media_load_selections');
        }
        return false;
    }
}
