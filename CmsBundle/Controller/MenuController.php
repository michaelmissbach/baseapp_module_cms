<?php

namespace Shape\CmsBundle\Controller;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Constant\NotifyType;
use BaseApp\BaseappBundle\Event\Tree\TypeFilter;
use BaseApp\BaseappBundle\Interfaces\IJsonApiCallable;
use BaseApp\BaseappBundle\Service\UserService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Media;
use Shape\CmsBundle\Interfaces\INodeRenderable;
use Shape\CmsBundle\Service\MediaService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Undocumented class
 */
class MenuController extends AbstractController implements IJsonApiCallable
{
    public function __construct(protected TranslatorInterface $translator)
    {}

    public function loadSelector(array $params): array
    {
        return [
            'title' => $this->translator->trans('baseapp.cms.menu.title.loadSelector'),
            'template' => $this->renderView('@Cms/menu/selector.html.twig', [])
        ];
    }

    /**
     * @param string $method
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function isJsonApiAllowed(string $method, array $params): bool
    {
        if ($method === 'loadSelector') {
            return UserService::$instance->isAllowed('cms_menu_load_selector');
        }
        return false;
    }
}
