<?php

namespace Shape\CmsBundle\Controller;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StructureController extends AbstractController
{
    public function __construct(protected TreeContainer $treeContainer) {}

    public function index(TreeContainer $container)
    {
        $guiBuilder = GuiBuilder::$instance;
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/baseapp/js/communicate.js'));
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/cms/js/structure.js'));
        $guiBuilder->getFooterStyleSheets()->add(InternStyleSheet::create()->setSource('/bundles/cms/css/structure.css'));

        foreach ($this->treeContainer->getNodesByTreeElementName('content') as $instance) {
            if (is_subclass_of($instance, INodeEditAssetsIncludeable::class)) {
                $instance::addEditorAssets($guiBuilder);
            }
        }

        return $this->render('@Cms/structure/index.html.twig',[]);
    }
}
