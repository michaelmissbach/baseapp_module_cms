<?php

namespace Shape\CmsBundle\Constant;

class NodeResponsableBehaviour
{
    const RENDER = 'render';
    const REDIRECT = 'redirect';
    const FIND_NEXT_CHILD = 'find_next_child';
}
