<?php

namespace Shape\CmsBundle\Constant;

class ContentType
{
    const GRID = 'grid';
    const CONTENT = 'content';
    const OTHER = 'other';
    const INTERN = 'intern';
    const SLIDER_SLIDE = 'slider_slide';
}
