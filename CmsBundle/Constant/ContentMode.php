<?php

namespace Shape\CmsBundle\Constant;

class ContentMode
{
    const DRAFT = 'draft';
    const PUBLISH = 'publish';
    const OLD = 'old';
}
