<?php

namespace Shape\CmsBundle\Constant;

class RendererMode
{
    const SHOW = 'show';
    const EDIT = 'edit';
}
