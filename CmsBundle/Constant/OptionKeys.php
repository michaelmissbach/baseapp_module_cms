<?php

namespace Shape\CmsBundle\Constant;

class OptionKeys
{
    const TEMPLATE = 'template';
    const TEMPLATE_KEY = 'template_key';
    const CONTAINER = 'container';
    const HIDDEN = 'hidden';
    const SHOW_IN_MENUS = 'show_in_menus';
    const USE_AS_URL_SEGMENT = 'use_as_url_segment';
    const URL_SEGMENT = 'url_segment';
    const TITLE = 'title';
    const ALT = 'alt';
    CONST MEDIA_ID = 'media_id';
    const MENU_ENTRY_ID = 'menu_entry_id';
    const MENU_TEMPLATE = 'menu_template';
    const REFERENCE_SOURCE = 'reference_source';
    const REFERENCE_TARGET = 'reference_target';
    const URL = 'url';
    const WIDTH = 'width';
    const HEIGHT = 'height';
    const ERROR_TYPE = 'error_type';
    const CONTROLS = 'controls';
    const INDICATORS = 'indicators';
    const FADES = 'fades';
    const INTERVAL = 'interval';
    const DARK = 'dark';
    const CONTENT = 'content';
}
