<?php

namespace Shape\CmsBundle\Service;

use BaseApp\BaseappBundle\Service\TreeService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Entity\Media;
use Shape\CmsBundle\Event\Media\Uploaded;
use Shape\CmsBundle\Interfaces\INodeMedia;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class MediaService
{
    public function __construct(protected EventDispatcherInterface $dispatcher,
                                protected TreeService $treeService,
                                protected TreeContainer $treeContainer,
                                protected EntityManagerInterface $doctrine)
    {}

    public static function getWebFolder(): string
    {
        return '/media';
    }

    public function upload(int $parentId, UploadedFile $uploadedFile): void
    {
        $event = Uploaded::create($parentId, $uploadedFile);
        $this->dispatcher->dispatch($event, Uploaded::NAME);
        if ($event->isProcessed()) {
            /** @var Media $media */
            $media = $this->treeService->addNode('media', $event->getMatchedNodename(), $parentId);
            $media->setOptions($event->getOptions());
            $media->setRaw($event->getRaw());
            $media->setIdentity(uniqid());
            $media->setExtension($event->getExtension());
            $this->doctrine->persist($media);
            $this->doctrine->flush();
        }
    }

    public function handleCacheElement($path): ?Response
    {
        $fileSegments = explode('/',$path);
        $filename = end($fileSegments);
        $extension = substr($filename, strrpos($filename, '.') + 1);
        $filename = substr($filename,0, strlen($filename)-(strlen($extension)+1));

        if ($media = $this->doctrine->getRepository(Media::class)
            ->findOneBy(['identity'=>$filename,'extension'=>$extension])) {
            $node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('media', $media);
            if ($node instanceof INodeMedia) {
                $content = $node->buildCache();
                return new Response($content);
            }
        }
        return null;
    }
}
