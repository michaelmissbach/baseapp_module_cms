<?php

namespace Shape\CmsBundle\Service;

use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Abstracts\AbstractNodeRoutable;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Model\Menu;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class MenuService
{
    protected bool $loaded = false;

    protected array $list = [];

    public function __construct(
        protected EntityManagerInterface $doctrine,
        protected TreeContainer $treeContainer,
        protected KernelInterface $kernel
    ) {}

    public function getCacheFileName(): string
    {
        return sprintf('%s/var/cache/menu/menu.cache', $this->kernel->getProjectDir());
    }

    protected function load(): void
    {
        if (!$this->loaded) {
            try {
                $content = file_get_contents($this->getCacheFileName());
                $this->list = unserialize($content);
                $this->loaded = true;
            } catch(\Exception $e) {
                $this->buildTree();
            }
        }
    }

    public function getMenuFromParentId($id): ?Menu
    {
        $this->load();

        if (array_key_exists((string)$id, $this->list['rootlines'])) {
            return $this->list['rootlines'][(string)$id];
        }
        return null;
    }

    public function buildTree()
    {
        $list = [];
        $structure = $this->doctrine->getRepository(Structure::class)->findOneByParent(null);
        $menu = $this->createMenuFromStructure($structure);
        $menu->setTitle('ROOT');
        $list[(string)$structure->getId()] = $menu;
        $this->buildTreeRecursive($structure, $menu, $list);
        $this->list = ['rootlines' => $list, 'menu'=>$menu];
        file_put_contents($this->getCacheFileName(), serialize($this->list));
        $this->loaded = true;
    }

    protected function buildTreeRecursive(Structure $structure, Menu $menu, array &$list): void
    {
        /** @var Structure $childStructure */
        foreach($structure->getChildren() as $childStructure) {
            $node = $this->treeContainer->getNodeClassnameByTreeElementNameAndNodename('structure', $childStructure->getNodeType());
            if (is_subclass_of($node, AbstractNodeRoutable::class)) {
                $hidden = $childStructure->getOption(OptionKeys::HIDDEN);
                $showInMenus = $childStructure->getOption(OptionKeys::SHOW_IN_MENUS);
                if (!$hidden && $showInMenus) {
                    $childMenu = $this->createMenuFromStructure($childStructure);
                    $childMenu->setParent($menu);
                    $menu->addChild($childMenu);
                    if ($childStructure->getChildren()->count()) {
                        $list[(string)$childStructure->getId()] = $childMenu;
                    }
                    $this->buildTreeRecursive($childStructure, $childMenu, $list);
                }
            }
        }
    }

    protected function createMenuFromStructure(Structure $structure): Menu
    {
        $menu = new Menu();
        $menu->setTitle($structure->getOption('title', 'not setted'));
        $menu->setUrl($structure->getBuiltUrl());
        $menu->setId($structure->getId());
        return $menu;
    }
}