<?php

namespace Shape\CmsBundle\Service;

use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Service\TreeService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Constant\RendererMode;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Event\Rendering\ErrorPage;
use Shape\CmsBundle\Interfaces\INodeRenderable;
use Shape\CmsBundle\Interfaces\ITreeRenderable;
use Shape\CmsBundle\Node\Content\SiteContent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class Renderer
{
    protected string $mode = RendererMode::SHOW;

    protected array $config = [];

    protected array $context = [];

    protected ParameterBag $renderBag;

    protected bool $siteCacheable = true;

    public function __construct(
        protected ParameterBagInterface $parameterBag,
        protected TreeContainer $treeContainer,
        protected Environment $twig,
        protected EventDispatcherInterface $dispatcher,
        protected TranslatorInterface $translator
    ) {
        $this->config = $parameterBag->get('cms');
        $this->renderBag = new ParameterBag();
    }

    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function render(ITreeRenderable $element): string
    {
        $content = '';
        if (!count($element->getChildren())){
            $content .= $this->renderInsert($element, TreeService::SORTING_STEP);
        } else {
            $first = true;
            /** @var ITreeRenderable $child */
            foreach ($element->getChildren() as $counter=>$child) {
                if (!$child->getOption(OptionKeys::HIDDEN, false)) {
                    /** @var INodeRenderable $node */
                    $node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('content', $child);

                    if ($first) {
                        if ($node->canInsertPreAndPostContent()) {
                            $content .= $this->renderInsert($element, TreeService::SORTING_STEP / 2);
                        }
                        $first = false;
                    }
                    $content .= $this->renderNode($node);
                    if (!$node->isCacheble()) {
                        $this->siteCacheable = false;
                    }
                    if ($node->canInsertPreAndPostContent()) {
                        $content .= $this->renderInsert($element, $child->getSorting() + (TreeService::SORTING_STEP / 2));
                    }
                }
            }
        }
        return $content;
    }

    public function renderNode(INodeRenderable $node): string
    {
        return $node->render($this->twig, $this->renderBag);
    }

    protected function renderInsert(ITree $element, int $sorting): string
    {
        if ($this->mode === RendererMode::EDIT) {
            return sprintf(
                '<div data-cms-insert="true" data-cms-content-parent="%s" data-cms-content-sorting="%s" data-cms-content-version="%s" data-cms-content-foreignid="%s"></div>',
                $element->getId(),
                $sorting,
                $element->getVersion(),
                $element->getForeignId()
            );
        }
        return '';
    }

    public function getTemplateConfigFromITree(ITree $element): ?array
    {
        return isset($this->config['templates'][$element->getOption(OptionKeys::TEMPLATE_KEY)])
            ? $this->config['templates'][$element->getOption(OptionKeys::TEMPLATE_KEY)]
            : null;
    }

    public function getTemplateFromITree(ITree $element): ?string
    {
        if ($element->hasOption(OptionKeys::TEMPLATE) && $element->getOption(OptionKeys::TEMPLATE)) {
            return $element->getOption(OptionKeys::TEMPLATE);
        } else if ($element->hasOption(OptionKeys::TEMPLATE_KEY) && $element->getOption(OptionKeys::TEMPLATE_KEY)) {
            if (isset($this->config['templates'][$element->getOption(OptionKeys::TEMPLATE_KEY)])) {
                return $this->config['templates'][$element->getOption(OptionKeys::TEMPLATE_KEY)]['template'];
            }
        }

        return null;
    }

    public function renderPage(ITree $element, ITreeRenderable $renderable): string
    {
        $this->renderBag->set('page', $element);

        /** @var ITree $child */
        foreach ($renderable->getChildren() as $child) {
            if ($child->getNodeType() === SiteContent::getName()) {
                if($template = $this->getTemplateFromITree($element)) {
                    return $this->twig->render($template, ['page' => $element, 'element' => $child]);
                } else {
                    $errorPage = ErrorPage::create(
                        $this->translator->trans('baseapp.cms.template.error.pagetemplatenotfound')
                    );
                    $this->dispatcher->dispatch($errorPage, ErrorPage::NAME);
                    return $errorPage->getRenderedPage();
                }
            }
        }
        return '';
    }

    public function isSiteCacheable(): bool
    {
        return $this->siteCacheable;
    }

    public function getRenderBag(): ParameterBag
    {
        return $this->renderBag;
    }
}
