<?php

namespace Shape\CmsBundle\Service;

use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Service\StringHelper;
use BaseApp\BaseappBundle\Service\TreeService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\NodeResponsableBehaviour;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Interfaces\INodeResponseable;
use Shape\CmsBundle\Interfaces\INodeRoutable;
use Shape\CmsBundle\Model\MatchedRoute;
use Shape\CmsBundle\Node\Structure\Domain;
use Shape\CmsBundle\Node\Structure\Error;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class Router
{
    public function __construct(
        protected TreeContainer $treeContainer,
        protected TreeService $treeService,
        protected EntityManagerInterface $doctrine
    )
    {}

    public function buildTree()
    {
        $connection = $this->doctrine->getConnection();
        $connection->executeStatement("UPDATE cms_structure SET built_url = '---'");

        if ($structure = $this->doctrine->getRepository(Structure::class)->findOneBy(['parent' => null])) {
            $this->buildTreeRecursive($structure);
        }
        $this->doctrine->flush();
    }

    protected function buildTreeRecursive(Structure $element, ?string $segments = ''): void
    {
        /** @var ITree $child */
        foreach ($element->getChildren() as $child) {
            if (!$child->getOption(OptionKeys::HIDDEN, false)) {
                /** @var INodeResponseable $node */
                $node = $this->treeContainer->getNodeClassnameByTreeElementNameAndNodename('structure', $child->getNodeType());
                if (is_subclass_of($node, INodeRoutable::class)) {
                    $newSegments = $segments;
                    if($child->getOption(OptionKeys::USE_AS_URL_SEGMENT, false)) {
                        if (strlen($newSegments)) {
                            $newSegments = sprintf('%s/', $newSegments);
                        }
                        $newSegments = sprintf('%s%s', $newSegments, $child->getOption(OptionKeys::URL_SEGMENT, ''));
                    }
                    $this->buildTreeRecursive($child, $newSegments);
                }
            }
        }

        if (StringHelper::endsWith($segments,'/')) {
            $segments = substr($segments,0, strlen($segments)-1);
        }
        $element->setBuiltUrl(strlen($segments)?$segments:'---');
        $this->doctrine->persist($element);
    }

    public function match(Request $request, string $path): MatchedRoute
    {
        $matchedRoute = new MatchedRoute();

        $matchRoute = sprintf('%s', $request->server->get('HTTP_HOST'));
        $segments = explode('/', $path);
        foreach ($segments as $segment) {
            if (!strlen($segment)) {
                continue;
            }
            $matchRoute = sprintf('%s/%s', $matchRoute, $segment);
        }

        $this->findInTree($matchRoute, $matchedRoute, null);

        return $matchedRoute;
    }

    protected function findInTree(string $matchRoute, MatchedRoute $matchedRoute, ?ITree $matchTree): void
    {
        if (!$matchTree) {
            if ($matchTree = $this->doctrine->getRepository(Structure::class)->findOneBy(['builtUrl'=>$matchRoute])) {
                if ($matchTree->getOption(OptionKeys::HIDDEN, false)) {
                    $matchTree = null;
                }
            }
        }
        if (!$matchTree) {
            return;
        }

        $node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('structure', $matchTree);
        if ($node instanceof INodeResponseable) {
            $currentFindMode = $node->getResponseBehaviour();
            switch($currentFindMode) {
                case NodeResponsableBehaviour::RENDER:
                    $matchedRoute->setMatchedPage($matchTree);
                    break;
                case NodeResponsableBehaviour::REDIRECT:
                    if ($redirectTarget = $this->doctrine->getRepository(Structure::class)->find((int)$matchTree->getOption(OptionKeys::MENU_ENTRY_ID))) {
                        if (!$redirectTarget->getOption(OptionKeys::HIDDEN, false)) {
                            $matchedRoute->setRedirectToPage($redirectTarget);
                        }
                    }
                    break;
                case NodeResponsableBehaviour::FIND_NEXT_CHILD:
                    if ($matchTree->getChildren()->count()) {
                        foreach($this->sortITreeElements($matchTree->getChildren()) as $child) {
                            if ($child->getOption(OptionKeys::HIDDEN, false)) {
                                continue;
                            }
                            $this->findInTree($matchRoute, $matchedRoute, $child);
                            break;
                        }
                    }
                    break;
                default:
                    throw new \Exception(sprintf('NodeResponsableBehaviour "%s" unknown. Route not matchable.', $currentFindMode));
            }
        }
    }

    public function matchErrorPage(Request $request, string $errorType): ?ITree
    {
        if ($domain = $this->findDomainInTreeByHost($request->server->get('HTTP_HOST'))) {
            if ($errorPage = $this->findErrorInTree($errorType, $domain)) {
                return $errorPage;
            }
        }
        return null;
    }

    protected function findDomainInTreeByHost(string $domain, ?ITree $parent = null): ?ITree
    {
        if (!$parent) {
            $parent = $this->doctrine->getRepository(Structure::class)->findOneBy(['parent' => null]);
        }

        if ($parent->getNodeType() === Domain::getName()) {
            if ($parent->getBuiltUrl() === $domain) {
                if (!$parent->getOption(OptionKeys::HIDDEN, false)) {
                    return $parent;
                }
            }
        }

        foreach ($this->sortITreeElements($parent->getChildren()) as $child) {
            if ($result = $this->findDomainInTreeByHost($domain, $child)) {
                return $result;
            }
        }
        return null;
    }

    protected function findErrorInTree(string $error, ?ITree $parent = null): ?ITree
    {
        if (!$parent) {
            $parent = $this->doctrine->getRepository(Structure::class)->findOneBy(['parent' => null]);
        }

        if ($parent->getNodeType() === Error::getName()) {
            if ($parent->getOption(OptionKeys::ERROR_TYPE, '') === $error) {
                if (!$parent->getOption(OptionKeys::HIDDEN, false)) {
                    return $parent;
                }
            }
        }

        foreach ($this->sortITreeElements($parent->getChildren()) as $child) {
            if ($result = $this->findErrorInTree($error, $child)) {
                return $result;
            }
        }
        return null;
    }

    protected function sortITreeElements(Collection $collection): array
    {
        $result = [];
        foreach ($collection as $entity) {
            $result[$entity->getSorting()] = $entity;
        }
        ksort($result);
        $result = array_values($result);
        return $result;
    }
}
