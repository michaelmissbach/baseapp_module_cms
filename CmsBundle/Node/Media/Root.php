<?php

namespace Shape\CmsBundle\Node\Media;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Root extends AbstractNode
{
    public function getTitle(): string
    {
        return 'baseapp.cms.media.node.title.root';
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.media.node.category.root';
    }

    public static function getName(): string
    {
        return 'root';
    }

    public static function getIcon(): string
    {
        return 'fas fa-network-wired';
    }

    public static function isDeleteable(): bool
    {
        return false;
    }

    public static function isCopyable(): bool
    {
        return false;
    }

    public static function isVisibleForSelection(): bool
    {
        return false;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
