<?php

namespace Shape\CmsBundle\Node\Media;

use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Interfaces\INodeMedia;
use Shape\CmsBundle\Service\MediaService;

abstract class AbstractMedia extends AbstractNodeRenderable implements INodeMedia
{
    protected function getBackendMediaFolder(): string
    {
        return sprintf('%s/public%s', $this->context->get('kernel')->getProjectDir(), MediaService::getWebFolder());
    }

    public function buildCache(): string
    {
        return '';
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
