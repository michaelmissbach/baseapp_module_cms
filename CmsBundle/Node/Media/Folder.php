<?php

namespace Shape\CmsBundle\Node\Media;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Constant\OptionKeys;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Folder extends AbstractNode
{
    public function getTitle(): string
    {
        if ($title = $this->getTreeElement()->getOption(OptionKeys::TITLE)) {
            return $title;
        }
        return 'baseapp.cms.media.node.title.folder';
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.media.node.category.folder';
    }

    public static function getName(): string
    {
        return 'folder';
    }

    public static function getIcon(): string
    {
        return 'fas fa-folder';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return false;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::TITLE, TextType::class, [
                'label' => 'baseapp.cms.options.title'
            ])
        ;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
