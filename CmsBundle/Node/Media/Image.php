<?php

namespace Shape\CmsBundle\Node\Media;

use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Service\MediaService;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Image extends AbstractMedia
{
    public static function getCategory(): string
    {
        return '';
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.media.node.title.image';
    }

    public static function getName(): string
    {
        return 'image';
    }

    public static function getIcon(): string
    {
        return 'fas fa-image';
    }

    public static function isVisibleInTree(): bool
    {
        return false;
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return false;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::TITLE, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::TITLE)
            ])
            ->add(OptionKeys::ALT, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::ALT)
            ])
        ;
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        $filename = sprintf('%s.%s', $this->treeElement->getIdentity(), $this->treeElement->getExtension());

        return $twig->render('@Cms/media/image.html.twig', [
            'element'=>$this->treeElement,
            'webFolder' => MediaService::getWebFolder(),
            'filename' => $filename,
            'title' => $this->treeElement->getOption(OptionKeys::TITLE),
            'alt' => $this->treeElement->getOption(OptionKeys::ALT)
        ]);
    }

    public function buildCache(): string
    {
        $filename = sprintf('%s.%s', $this->treeElement->getIdentity(), $this->treeElement->getExtension());
        $fullPath = sprintf('%s/%s', $this->getBackendMediaFolder(), $filename);
        $content = base64_decode($this->treeElement->getRaw());
        file_put_contents($fullPath, $content);
        return $content;
    }
}
