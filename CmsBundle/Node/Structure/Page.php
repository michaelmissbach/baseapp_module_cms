<?php

namespace Shape\CmsBundle\Node\Structure;

use Shape\CmsBundle\Abstracts\AbstractNodeRoutable;
use Shape\CmsBundle\Constant\NodeResponsableBehaviour;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Interfaces\INodeResponseable;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Page extends AbstractNodeRoutable implements INodeResponseable
{
    public function getTitle(): string
    {
        if ($title = $this->treeElement->getOption(OptionKeys::TITLE)) {
            return $title;
        }
        if ($title = $this->treeElement->getOption(OptionKeys::URL_SEGMENT)) {
            return $title;
        }
        return 'baseapp.cms.structure.node.title.page';
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.structure.node.category.page';
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            'baseapp.cms.structure.node.category.folder',
            'baseapp.cms.structure.node.category.page'
        ];
    }

    public static function getName(): string
    {
        return 'page';
    }

    public static function getIcon(): string
    {
        return 'fas fa-file';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        parent::optionsFormBuilder($formBuilder, $parameterBag);

        $formBuilder
            ->add(OptionKeys::URL_SEGMENT, TextType::class, [
                'label' => 'baseapp.cms.options.urlsegment'
            ])
            ->add(OptionKeys::TITLE, TextType::class, [
                'label' => 'baseapp.cms.options.title'
            ])
        ;

        $choices = [];
        foreach ($parameterBag->get('config')['templates'] as $key=>$values) {
            $choices[$key] = $key;
        }
        $formBuilder
            ->add(OptionKeys::TEMPLATE_KEY, ChoiceType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::TEMPLATE_KEY),
                'placeholder' => 'baseapp.common.pleasechoose',
                'choices' => $choices
            ])
        ;
    }

    public function getResponseBehaviour(): string
    {
        return NodeResponsableBehaviour::RENDER;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
