<?php

namespace Shape\CmsBundle\Node\Structure;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Abstracts\AbstractNodeRoutable;
use Shape\CmsBundle\Constant\NodeResponsableBehaviour;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Interfaces\INodeResponseable;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Domain extends AbstractNodeRoutable implements INodeResponseable
{
    public function getTitle(): string
    {
        $options = $this->getTreeElement()->getOptions();
        if (isset($options[OptionKeys::URL_SEGMENT]) && strlen($options[OptionKeys::URL_SEGMENT])) {
            $title = $options[OptionKeys::URL_SEGMENT];
        } else {
            $title = 'baseapp.cms.structure.node.title.domain';
        }
        return $title;
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.structure.node.category.domain';
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            'baseapp.cms.structure.node.category.folder',
            'baseapp.cms.structure.node.category.page'
        ];
    }

    public static function getName(): string
    {
        return 'domain';
    }

    public static function getIcon(): string
    {
        return 'fas fa-globe';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        parent::optionsFormBuilder($formBuilder, $parameterBag);

        $formBuilder
            ->add(OptionKeys::URL_SEGMENT, TextType::class, [
                'label' => 'baseapp.cms.options.urlsegment'
            ])
            ;
    }

    public function getResponseBehaviour(): string
    {
        return NodeResponsableBehaviour::FIND_NEXT_CHILD;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
