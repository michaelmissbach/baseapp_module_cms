<?php

namespace Shape\CmsBundle\Node\Structure;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Constant\NodeResponsableBehaviour;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Interfaces\INodeResponseable;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Referencesource extends AbstractNode implements INodeResponseable
{
    public function getTitle(): string
    {
        if ($title = $this->treeElement->getOption(OptionKeys::TITLE)) {
            return $title;
        }
        return 'baseapp.cms.structure.node.title.referencesource';
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.structure.node.category.page';
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            'baseapp.cms.structure.node.category.folder',
            'baseapp.cms.structure.node.category.page'
        ];
    }

    public static function getName(): string
    {
        return 'referencesource';
    }

    public static function getIcon(): string
    {
        return 'fas fa-anchor';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::HIDDEN, CheckboxType::class, [
                'label' => 'baseapp.cms.options.hidden'
            ])
            ->add(OptionKeys::TITLE, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s',OptionKeys::TITLE)
            ])
        ;
    }

    public function getResponseBehaviour(): string
    {
        return NodeResponsableBehaviour::RENDER;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
