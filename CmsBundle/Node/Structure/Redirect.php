<?php

namespace Shape\CmsBundle\Node\Structure;

use Shape\CmsBundle\Abstracts\AbstractNodeRoutable;
use Shape\CmsBundle\Constant\NodeResponsableBehaviour;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Interfaces\INodeResponseable;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Redirect extends AbstractNodeRoutable implements INodeResponseable
{
    public function getTitle(): string
    {
        return 'baseapp.cms.structure.node.title.redirect';
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.structure.node.category.page';
    }

    public static function getName(): string
    {
        return 'redirect';
    }

    public static function getIcon(): string
    {
        return 'fas fa-chevron-circle-right';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        parent::optionsFormBuilder($formBuilder, $parameterBag);

        $formBuilder
            ->add(OptionKeys::MENU_ENTRY_ID, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::MENU_ENTRY_ID)
            ])
            ->add(OptionKeys::URL_SEGMENT, TextType::class, [
                'label' => 'baseapp.cms.options.urlsegment'
            ])
            ->add(OptionKeys::TITLE, TextType::class, [
                'label' => 'baseapp.cms.options.title'
            ])
        ;
    }

    public function getResponseBehaviour(): string
    {
        return NodeResponsableBehaviour::REDIRECT;
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
