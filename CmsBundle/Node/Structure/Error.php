<?php

namespace Shape\CmsBundle\Node\Structure;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Constant\OptionKeys;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Error extends AbstractNode
{
    public function getTitle(): string
    {
        $title = $this->context->get('translator')->trans('baseapp.cms.structure.node.title.error');
        if ($error = $this->treeElement->getOption(OptionKeys::ERROR_TYPE)) {
            $title = sprintf('%s %s', $title, $error);
        }
        return $title;
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.structure.node.category.page';
    }

    public static function getName(): string
    {
        return 'error';
    }

    public static function getIcon(): string
    {
        return 'fas fa-bug';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::HIDDEN, CheckboxType::class, [
                'label' => 'baseapp.cms.options.hidden'
            ])
            ->add(OptionKeys::ERROR_TYPE, ChoiceType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::ERROR_TYPE),
                'placeholder' => 'baseapp.common.pleasechoose',
                'choices' => [
                    $this->context->get('translator')->trans('baseapp.cms.errorpage.404.title') => '404',
                    $this->context->get('translator')->trans('baseapp.cms.errorpage.500.title') => '500'
                ]
            ])
        ;

        $choices = [];
        foreach ($parameterBag->get('config')['templates'] as $key=>$values) {
            $choices[$key] = $key;
        }
        $formBuilder
            ->add(OptionKeys::TEMPLATE_KEY, ChoiceType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::TEMPLATE_KEY),
                'placeholder' => 'baseapp.common.pleasechoose',
                'choices' => $choices
            ])
        ;
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
