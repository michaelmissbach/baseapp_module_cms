<?php

namespace Shape\CmsBundle\Node\Structure;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Abstracts\AbstractNodeRoutable;
use Shape\CmsBundle\Constant\NodeResponsableBehaviour;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Interfaces\INodeResponseable;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Folder extends AbstractNodeRoutable implements INodeResponseable
{
    public function getTitle(): string
    {
        if ($title = $this->treeElement->getOption(OptionKeys::TITLE)) {
            return $title;
        }
        return 'baseapp.cms.structure.node.title.folder';
    }

    public static function getCategory(): string
    {
        return 'baseapp.cms.structure.node.category.folder';
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            'baseapp.cms.structure.node.category.folder',
            'baseapp.cms.structure.node.category.page'
        ];
    }

    public static function getName(): string
    {
        return 'folder';
    }

    public static function getIcon(): string
    {
        return 'fas fa-folder';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        parent::optionsFormBuilder($formBuilder, $parameterBag);

        $formBuilder
            ->add(OptionKeys::URL_SEGMENT, TextType::class, [
                'label' => 'baseapp.cms.options.urlsegment'
            ])
            ->add(OptionKeys::TITLE, TextType::class, [
                'label' => 'baseapp.cms.options.title'
            ])
        ;
    }

    public function getResponseBehaviour(): string
    {
        return NodeResponsableBehaviour::FIND_NEXT_CHILD;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
