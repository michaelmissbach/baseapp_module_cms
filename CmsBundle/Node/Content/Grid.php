<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Grid extends AbstractNodeRenderable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::GRID);
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            sprintf('baseapp.content.node.category.%s',ContentType::GRID),
            sprintf('baseapp.content.node.category.%s',ContentType::CONTENT),
            sprintf('baseapp.content.node.category.%s',ContentType::OTHER),
        ];
    }

    public function getTitle(): string
    {
        return sprintf('%s %s%s',
            $this->context->get('translator')->trans('baseapp.cms.content.node.title.grid'),
            (string)$this->treeElement->getOption('percent'),
        '%');
    }

    public static function getName(): string
    {
        return 'grid';
    }

    public static function getIcon(): string
    {
        return 'fas fa-table';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'grid.html.twig'),
            [
                'element'=>$this->treeElement,
                'page' => $parameters->get('page'),
                'percent'=>$this->treeElement->getOption('percent')
            ]
        );
    }

    public function canRenderChildren(): bool
    {
        return true;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }

    public function canInsertPreAndPostContent(): bool
    {
        return false;
    }
}
