<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Structure;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Container extends AbstractNodeRenderable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::INTERN);
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            sprintf('baseapp.content.node.category.%s',ContentType::GRID)
        ];
    }

    public function getTitle(): string
    {
        $title = $this->context->get('translator')->trans('baseapp.cms.content.node.title.container');
        $options = $this->getTreeElement()->getOptions();
        if (isset($options[OptionKeys::CONTAINER]) && strlen($options[OptionKeys::CONTAINER])) {
            $title = sprintf('%s: %s', $title, $options[OptionKeys::CONTAINER]);
        }
        return $title;
    }

    public static function getName(): string
    {
        return 'container';
    }

    public static function getIcon(): string
    {
        return 'fas fa-folder';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $structure =
            $parameterBag->get('doctrine')
                ->getRepository(Structure::class)
                ->find((int)$this->getTreeElement()->getForeignId());
        $treeOptions = $structure->getOptions();
        if (isset($treeOptions[OptionKeys::TEMPLATE_KEY]) && strlen($treeOptions[OptionKeys::TEMPLATE_KEY])) {
            $choices = [];
            foreach ($parameterBag->get('config')['templates'][$treeOptions[OptionKeys::TEMPLATE_KEY]]['containers'] as $key=>$value) {
                $choices[$value] = $value;
            }
            $formBuilder
                ->add(OptionKeys::CONTAINER, ChoiceType::class, [
                    'label' => sprintf('baseapp.cms.options.%s', OptionKeys::CONTAINER),
                    'placeholder' => 'baseapp.common.pleasechoose',
                    'choices' => $choices
                ])
            ;
        }

    }

    public function canRenderChildren(): bool
    {
        return true;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
