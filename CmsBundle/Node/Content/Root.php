<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Root extends AbstractNodeRenderable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::INTERN);
    }

    public static function allowedChildCategories(): ?array
    {
        return [];
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.root';
    }

    public static function getName(): string
    {
        return 'root';
    }

    public static function getIcon(): string
    {
        return 'fas fa-network-wired';
    }

    public static function isDeleteable(): bool
    {
        return false;
    }

    public static function isCopyable(): bool
    {
        return false;
    }

    public static function isVisibleForSelection(): bool
    {
        return false;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }
}
