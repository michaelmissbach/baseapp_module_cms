<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Constant\ContentType;

class Grid1 extends Row
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::GRID);
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            sprintf('baseapp.content.node.category.%s',ContentType::GRID),
            sprintf('baseapp.content.node.category.%s',ContentType::CONTENT),
            sprintf('baseapp.content.node.category.%s',ContentType::OTHER),
        ];
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.grids';
    }

    public static function getName(): string
    {
        return 'grid1';
    }

    public static function getIcon(): string
    {
        return 'fas fa-columns';
    }
}
