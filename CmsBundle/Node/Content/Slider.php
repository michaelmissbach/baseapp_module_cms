<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Slider extends AbstractNodeRenderable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::CONTENT);
    }

    public static function allowedChildCategories(): ?array
    {
        return [
            sprintf('baseapp.content.node.category.%s',ContentType::SLIDER_SLIDE)
        ];
    }

    public function getTitle(): string
    {
        return $this->context->get('translator')->trans('baseapp.cms.content.node.title.slider');
    }

    public static function getName(): string
    {
        return 'slider';
    }

    public static function getIcon(): string
    {
        return 'far fa-clone';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::HIDDEN, CheckboxType::class, [
                'label' => 'baseapp.cms.options.hidden'
            ])
            ->add(OptionKeys::CONTROLS, CheckboxType::class, [
                'label' => 'baseapp.cms.options.controls'
            ])
            ->add(OptionKeys::INDICATORS, CheckboxType::class, [
                'label' => 'baseapp.cms.options.indicators'
            ])
            ->add(OptionKeys::FADES, CheckboxType::class, [
                'label' => 'baseapp.cms.options.fades'
            ])
            ->add(OptionKeys::DARK, CheckboxType::class, [
                'label' => 'baseapp.cms.options.dark'
            ])
            ->add(OptionKeys::INTERVAL, TextType::class, [
                'label' => 'baseapp.cms.options.interval'
            ])
            ->add(OptionKeys::WIDTH, TextType::class, [
                'label' => 'baseapp.cms.options.width'
            ])
            ->add(OptionKeys::HEIGHT, TextType::class, [
                'label' => 'baseapp.cms.options.height'
            ])
        ;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'slider.html.twig'),
            [
                'element'=>$this->treeElement,
                'controls' => $this->treeElement->getOption(OptionKeys::CONTROLS, false),
                'indicators' => $this->treeElement->getOption(OptionKeys::INDICATORS, false),
                'fades' => $this->treeElement->getOption(OptionKeys::FADES, false),
                'dark' => $this->treeElement->getOption(OptionKeys::DARK, false),
                'interval' => $this->treeElement->getOption(OptionKeys::INTERVAL, false),
            ]
        );
    }

    public function canRenderChildren(): bool
    {
        return true;
    }

    public static function canHaveChildren(): bool
    {
        return true;
    }

    public function canInsertPreAndPostContent(): bool
    {
        return true;
    }
}
