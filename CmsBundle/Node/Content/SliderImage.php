<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Media;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;
use Shape\CmsBundle\Form\ImageType;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Shape\CmsBundle\Service\MediaService;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class SliderImage extends Image
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::SLIDER_SLIDE);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.slider_image';
    }

    public static function getName(): string
    {
        return 'slider_image';
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        parent::optionsFormBuilder($formBuilder, $parameterBag);
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        $parent = $this->treeElement->getParent();

        $options = [
            'element'=>$this->treeElement,
            'webFolder' => MediaService::getWebFolder(),
            'filename' => null,
            'title' => '',
            'alt' => '',
            'width' => $parent->getOption(OptionKeys::WIDTH),
            'height' => $parent->getOption(OptionKeys::HEIGHT)
        ];

        if ($id = $this->treeElement->getOption(OptionKeys::MEDIA_ID))
        {
            /** @var Media $media */
            if ($media = $this->context->get('doctrine')->getRepository(Media::class)->find((int)$id)) {
                $options['filename'] = sprintf('%s.%s', $media->getIdentity(), $media->getExtension());
                $options['title'] = $media->getOption(OptionKeys::TITLE);
                $options['alt'] = $media->getOption(OptionKeys::ALT);
            }
        }

        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'slider_image.html.twig'),
            $options
        );
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }

    public static function addEditorAssets(GuiBuilder $guiBuilder): void
    {
    }

    public static function addInPlaceAssets(AbstractRendering $event): void
    {
    }
}
