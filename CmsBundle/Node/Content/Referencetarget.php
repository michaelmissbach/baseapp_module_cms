<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentMode;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Constant\RendererMode;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Node\Structure\Referencesource;
use Shape\CmsBundle\Service\Renderer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Referencetarget extends AbstractNodeRenderable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::OTHER);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.referencetarget';
    }

    public static function getName(): string
    {
        return 'referencetarget';
    }

    public static function getIcon(): string
    {
        return 'fas fa-anchor';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $entities =
            $this->context->get('doctrine')->getRepository(Structure::class)
                ->findByNodeType(Referencesource::getName());
        $choices = [];
        foreach ($entities as $entity) {
            $name = $entity->getOption(OptionKeys::TITLE);
            if ($name) {
                $choices[$name] = $entity->getId();
            }
        }
        $formBuilder
            ->add(OptionKeys::REFERENCE_SOURCE, ChoiceType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::REFERENCE_SOURCE),
                'placeholder' => 'baseapp.common.pleasechoose',
                'choices' => $choices
            ])
        ;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        $renderedContent = null;
        if ($id = $this->treeElement->getOption(OptionKeys::REFERENCE_SOURCE)) {
            if ($referenceSource = $this->context->get('doctrine')->getRepository(Structure::class)->find((int)$id)) {
                if (!$referenceSource->getOption(OptionKeys::HIDDEN)) {
                    /** @var Content $content */
                    if ($content = $this->context->get('doctrine')->getRepository(Content::class)->findOneBy(['foreignId' => $id, 'parent' => null, 'mode' => ContentMode::PUBLISH])) {
                        /** @var ITree $child */
                        foreach ($content->getChildren() as $child) {
                            if ($child->getNodeType() === SiteContent::getName()) {
                                $renderedContent = self::renderReferenceContent($this->context->get('renderer'), $child);
                            }
                        }
                    }
                }
            }
        }
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'referencetarget.html.twig'),
            ['element' => $this->treeElement, 'content' => $renderedContent]);
    }

    public static function renderReferenceContent(Renderer $renderer, Content $content): string
    {
        $mode = $renderer->getMode();
        $renderer->setMode(RendererMode::SHOW);
        $renderedContent = $renderer->getTwig()->render('@Cms/content/referencetarget_content.html.twig', [
                'element' => $content
            ]
        );
        $renderer->setMode($mode);
        return $renderedContent;
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
