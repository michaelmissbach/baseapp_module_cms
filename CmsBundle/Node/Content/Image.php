<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Media;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;
use Shape\CmsBundle\Form\ImageType;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Shape\CmsBundle\Service\MediaService;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Image extends AbstractNodeRenderable implements INodeEditAssetsIncludeable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::CONTENT);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.image';
    }

    public static function getName(): string
    {
        return 'image';
    }

    public static function getIcon(): string
    {
        return 'fas fa-image';
    }

    public static function isVisibleInTree(): bool
    {
        return true;
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $filename = null;
        if ($id = $this->treeElement->getOption(OptionKeys::MEDIA_ID))
        {
            /** @var Media $media */
            if ($media = $this->context->get('doctrine')->getRepository(Media::class)->find((int)$id)) {
                $filename = sprintf('%s.%s', $media->getIdentity(), $media->getExtension());
            }
        }
        $formBuilder
            ->add(OptionKeys::MEDIA_ID, ImageType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::MEDIA_ID),
                'filename' => sprintf('%s/%s', MediaService::getWebFolder(),$filename),
                'media_id' => $this->treeElement->getId(),
                'media_type' => 'image'
            ])
        ;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        $options = [
            'element'=>$this->treeElement,
            'webFolder' => MediaService::getWebFolder(),
            'filename' => null,
            'title' => '',
            'alt' => ''
        ];

        if ($id = $this->treeElement->getOption(OptionKeys::MEDIA_ID))
        {
            /** @var Media $media */
            if ($media = $this->context->get('doctrine')->getRepository(Media::class)->find((int)$id)) {
                $options['filename'] = sprintf('%s.%s', $media->getIdentity(), $media->getExtension());
                $options['title'] = $media->getOption(OptionKeys::TITLE);
                $options['alt'] = $media->getOption(OptionKeys::ALT);
            }
        }

        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'image.html.twig'),
            $options
        );
    }

    public function renderAdditionalEditTags(): string
    {
        return 'data-cms-media="true"';
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function addEditorAssets(GuiBuilder $guiBuilder): void
    {
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/cms/js/editor-media.js'));
    }

    public static function addInPlaceAssets(AbstractRendering $event): void
    {
        $event->addContent('<script src="/bundles/cms/js/in_place-media.js"></script>');
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
