<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Menu extends AbstractNodeRenderable implements INodeEditAssetsIncludeable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::OTHER);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.menu';
    }

    public static function getName(): string
    {
        return 'menu';
    }

    public static function getIcon(): string
    {
        return 'fas fa-bars';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::MENU_ENTRY_ID, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::MENU_ENTRY_ID)
            ])
            ;
        $choices = [];
        foreach ($parameterBag->get('config')['menus'] as $key=>$values) {
            $choices[$key] = $values['template'];
        }
        $formBuilder
            ->add(OptionKeys::MENU_TEMPLATE, ChoiceType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::MENU_TEMPLATE),
                'placeholder' => 'baseapp.common.pleasechoose',
                'choices' => $choices
            ])
        ;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        $renderedContent = null;
        if ($template = $this->treeElement->getOption(OptionKeys::MENU_TEMPLATE)) {
            if ($id = $this->treeElement->getOption(OptionKeys::MENU_ENTRY_ID)) {
                if ($menu = $this->context->get('menuService')->getMenuFromParentId($id)) {
                    $renderedContent = $twig->render($template, array_merge($this->context->all(),[
                        'element' => $this->treeElement,
                        'page' => $parameters->get('page'),
                        'menu' => $menu
                    ]));
                }
            }
        }
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'menu.html.twig'),
            ['element'=>$this->treeElement,'content' => $renderedContent]
        );
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function addEditorAssets(GuiBuilder $guiBuilder): void
    {
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/cms/js/editor-menu.js'));
    }

    public static function addInPlaceAssets(AbstractRendering $event): void
    {
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
