<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Constant\ContentType;

class Grid3 extends Grid1
{
    public static function getName(): string
    {
        return 'grid3';
    }
}
