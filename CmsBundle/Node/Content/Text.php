<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Text extends AbstractNodeRenderable implements INodeEditAssetsIncludeable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::CONTENT);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.text';
    }

    public static function getName(): string
    {
        return 'text';
    }

    public static function getIcon(): string
    {
        return 'fas fa-font';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'text.html.twig'),
            ['element'=>$this->treeElement]
        );
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function addEditorAssets(GuiBuilder $guiBuilder): void
    {
    }

    public static function addInPlaceAssets(AbstractRendering $event): void
    {
        $event->addContent('<script src="/bundles/cms/js/in_place-text.js"></script>');
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
