<?php

namespace Shape\CmsBundle\Node\Content;

class Grid2 extends Grid1
{
    public static function getName(): string
    {
        return 'grid2';
    }
}
