<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class YouTube extends AbstractNodeRenderable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::CONTENT);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.youtube';
    }

    public static function getName(): string
    {
        return 'youtube';
    }

    public static function getIcon(): string
    {
        return 'fas fa-image';
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::URL, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::URL)
            ])
            ->add(OptionKeys::WIDTH, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::WIDTH)
            ])
            ->add(OptionKeys::HEIGHT, TextType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::HEIGHT)
            ])
        ;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'youtube.html.twig'),
            ['element'=>$this->treeElement]
        );
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }

}
