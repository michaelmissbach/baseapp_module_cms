<?php

namespace Shape\CmsBundle\Node\Content;

use Shape\CmsBundle\Constant\ContentMode;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class SliderReferencetarget extends Referencetarget
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::SLIDER_SLIDE);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.slider_referencetarget';
    }

    public static function getName(): string
    {
        return 'slider_referencetarget';
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        $renderedContent = null;
        if ($id = $this->treeElement->getOption(OptionKeys::REFERENCE_SOURCE)) {
            if ($referenceSource = $this->context->get('doctrine')->getRepository(Structure::class)->find((int)$id)) {
                if (!$referenceSource->getOption(OptionKeys::HIDDEN)) {
                    /** @var Content $content */
                    if ($content = $this->context->get('doctrine')->getRepository(Content::class)->findOneBy(['foreignId' => $id, 'parent' => null, 'mode' => ContentMode::PUBLISH])) {
                        /** @var ITree $child */
                        foreach ($content->getChildren() as $child) {
                            if ($child->getNodeType() === SiteContent::getName()) {
                                $renderedContent = self::renderReferenceContent($this->context->get('renderer'), $child);
                            }
                        }
                    }
                }
            }
        }
        $parent = $this->treeElement->getParent();
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'slider_referencetarget.html.twig'),
            [
                'element' => $this->treeElement,
                'content' => $renderedContent,
                'width' => $parent->getOption(OptionKeys::WIDTH),
                'height' => $parent->getOption(OptionKeys::HEIGHT)
            ]
        );
        //https://getbootstrap.com/docs/5.0/components/carousel/
    }
}
