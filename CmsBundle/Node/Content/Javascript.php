<?php

namespace Shape\CmsBundle\Node\Content;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Abstracts\AbstractNodeRenderable;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

class Javascript extends AbstractNodeRenderable implements INodeEditAssetsIncludeable
{
    public static function getCategory(): string
    {
        return sprintf('baseapp.content.node.category.%s',ContentType::CONTENT);
    }

    public function getTitle(): string
    {
        return 'baseapp.cms.content.node.title.javascript';
    }

    public static function getName(): string
    {
        return 'javascript';
    }

    public static function getIcon(): string
    {
        return 'fab fa-js-square';
    }

    public static function isVisibleInTree(): bool
    {
        return true;
    }

    public static function isDeleteable(): bool
    {
        return true;
    }

    public static function isCopyable(): bool
    {
        return true;
    }

    public static function isVisibleForSelection(): bool
    {
        return true;
    }

    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::CONTENT, TextareaType::class, [
                'label' => sprintf('baseapp.cms.options.%s', OptionKeys::CONTENT)
            ])
        ;
    }

    public function render(Environment $twig, ParameterBag $parameters): string
    {
        return $twig->render(
            $this->buildTemplatePath($parameters->get('page'), 'javascript.html.twig'),
            [
                'element' => $this->treeElement,
                'content' => $this->treeElement->getOption(OptionKeys::CONTENT)
            ]
        );
    }

    public function renderAdditionalEditTags(): string
    {
        return 'data-cms-media="true"';
    }

    public function canRenderChildren(): bool
    {
        return false;
    }

    public static function addEditorAssets(GuiBuilder $guiBuilder): void
    {
        $guiBuilder->getFooterJavascripts()->add(InternJavascript::create()->setSource('/bundles/cms/js/editor-javascript.js'));
    }

    public static function addInPlaceAssets(AbstractRendering $event): void
    {
    }

    public static function canHaveChildren(): bool
    {
        return false;
    }
}
