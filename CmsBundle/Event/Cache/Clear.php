<?php

namespace Shape\CmsBundle\Event\Cache;

use Symfony\Contracts\EventDispatcher\Event;

class Clear extends Event
{
    const NAME = 'baseapp.cms.cache.clear';

    protected array $messages = [];

    protected function __construct()
    {
    }

    public static function create(): static
    {
        return new static();
    }

    public function addMessage(string $message): void
    {
        $this->messages[] = $message;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }
}
