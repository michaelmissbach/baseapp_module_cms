<?php

namespace Shape\CmsBundle\Event\Media;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Contracts\EventDispatcher\Event;

class Uploaded extends Event
{
    const NAME = 'baseapp.cms.media.uploaded.file';

    protected string $extension;

    protected bool $processed = false;

    protected string $raw = '';

    protected array $options = [];

    protected string $matchedNodename = '';

    protected function __construct(protected int $parentId, protected UploadedFile $uploadedFile)
    {
        $this->extension = $this->uploadedFile->getClientOriginalExtension();
    }

    public static function create(int $parentId, UploadedFile $uploadedFile): static
    {
        return new static($parentId, $uploadedFile);
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function isProcessed(): bool
    {
        return $this->processed;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function getUploadedFile(): UploadedFile
    {
        return $this->uploadedFile;
    }

    public function setProcessed(bool $processed): void
    {
        $this->processed = $processed;
    }

    public function setRaw(string $raw): void
    {
        $this->raw = $raw;
    }

    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    public function getMatchedNodename(): string
    {
        return $this->matchedNodename;
    }

    public function setMatchedNodename(string $matchedNodename): void
    {
        $this->matchedNodename = $matchedNodename;
    }

    public function getRaw(): string
    {
        return $this->raw;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
