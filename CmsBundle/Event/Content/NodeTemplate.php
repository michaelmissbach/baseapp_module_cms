<?php

namespace Shape\CmsBundle\Event\Content;

use BaseApp\BaseappBundle\Interfaces\ITree;
use Symfony\Contracts\EventDispatcher\Event;

class NodeTemplate extends Event
{
    const NAME = 'baseapp.cms.content.node.template';

    protected string $builtTemplatePath = '';

    protected function __construct(protected ITree $page, protected string $template)
    {}

    public static function create(ITree $page, string $template): static
    {
        return new static($page, $template);
    }

    public function getBuiltTemplatePath(): string
    {
        return $this->builtTemplatePath;
    }

    public function setBuiltTemplatePath(string $builtTemplatePath): void
    {
        $this->builtTemplatePath = $builtTemplatePath;
    }

    public function getPage(): ITree
    {
        return $this->page;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }
}
