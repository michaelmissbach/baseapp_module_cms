<?php

namespace Shape\CmsBundle\Event\Rendering;

use Symfony\Contracts\EventDispatcher\Event;

class ErrorPage extends Event
{
    const NAME = 'baseapp.cms.rendering.error.page';

    protected $renderedPage = '';

    protected function __construct(protected string $message)
    {
    }

    public static function create(string $message): static
    {
        return new static($message);
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getRenderedPage(): string
    {
        return $this->renderedPage;
    }

    public function setRenderedPage(string $renderedPage): void
    {
        $this->renderedPage = $renderedPage;
    }
}
