<?php

namespace Shape\CmsBundle\Event\Rendering;

class PreBodyContent extends AbstractRendering
{
    const NAME = 'baseapp.cms.rendering.pre.body.content';
}
