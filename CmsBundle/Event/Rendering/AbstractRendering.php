<?php

namespace Shape\CmsBundle\Event\Rendering;

use BaseApp\BaseappBundle\Interfaces\ITree;
use Symfony\Contracts\EventDispatcher\Event;

abstract class AbstractRendering extends Event
{
    protected string $content = "";

    /**
     *
     */
    protected function __construct(protected ITree $page)
    {
    }

    /**
     * @return $this
     */
    public static function create(ITree $page): static
    {
        return new static($page);
    }

    public function getPage(): ITree
    {
        return $this->page;
    }

    /**
     * @param string $content
     * @return void
     */
    public function addContent(string $content): void
    {
        $this->content .= $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
