<?php

namespace Shape\CmsBundle\Event\Rendering;

class PostBodyContent extends AbstractRendering
{
    const NAME = 'baseapp.cms.rendering.post.body.content';
}
