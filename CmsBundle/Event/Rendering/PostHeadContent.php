<?php

namespace Shape\CmsBundle\Event\Rendering;

class PostHeadContent extends AbstractRendering
{
    const NAME = 'baseapp.cms.rendering.post.head.content';
}
