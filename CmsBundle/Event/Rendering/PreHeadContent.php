<?php

namespace Shape\CmsBundle\Event\Rendering;

class PreHeadContent extends AbstractRendering
{
    const NAME = 'baseapp.cms.rendering.pre.head.content';
}
