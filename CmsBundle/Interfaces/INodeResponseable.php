<?php

namespace Shape\CmsBundle\Interfaces;

interface INodeResponseable
{
    public function getResponseBehaviour(): string;
}
