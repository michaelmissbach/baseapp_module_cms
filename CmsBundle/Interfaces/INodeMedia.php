<?php

namespace Shape\CmsBundle\Interfaces;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;

interface INodeMedia
{
    public function buildCache(): string;
}
