<?php

namespace Shape\CmsBundle\Interfaces;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use Shape\CmsBundle\Event\Rendering\AbstractRendering;

interface INodeEditAssetsIncludeable
{
    public static function addEditorAssets(GuiBuilder $guiBuilder): void;

    public static function addInPlaceAssets(AbstractRendering $event): void;
}
