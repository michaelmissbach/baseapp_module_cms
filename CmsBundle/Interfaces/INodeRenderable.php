<?php

namespace Shape\CmsBundle\Interfaces;

use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

interface INodeRenderable
{
    public function render(Environment $twig, ParameterBag $parameters): string;

    public function renderAdditionalEditTags(): string;

    public function canRenderChildren(): bool;

    public function canInsertPreAndPostContent(): bool;

    public function isCacheble(): bool;
}