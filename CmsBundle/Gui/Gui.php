<?php

namespace Shape\CmsBundle\Gui;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level1Divider;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level1MenuEntryWithChilds;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level1MenuEntryWithChildsLeft;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level2MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;
use BaseApp\BaseappBundle\Gui\Elements\Alerts;
use BaseApp\BaseappBundle\Gui\Elements\Messages;
use BaseApp\BaseappBundle\Service\AppService;
use BaseApp\BaseappBundle\Service\UserService;
use Shape\CmsBundle\Gui\Elements\Cache;
use Shape\CmsBundle\Gui\Elements\Publish;

/**
 * Undocumented class
 */
class Gui implements IGui
{/**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void
    {   
        $guiBuilder->setShowSidebar(false);
        $guiBuilder->setShowFooter(false);

        $topMenu = $guiBuilder->getTopMenuLeft();
        $container = $guiBuilder->createGuiContainer();
        $topMenu->add(
            Level1MenuEntryWithChildsLeft::create()
                ->setChildContainer($container)
                ->setTitle('Shape')
                ->setName('baseapp_cms_menu')
                ->setIcon('fa fa-indent fa-sm fa-fw mr-2 text-gray-400'),
            0
        );
        if (UserService::$instance->isAllowed('cms_structure')) {
            $container->add(
                Level2MenuEntry::create()
                    ->setTitle('baseapp.cms.structure.menu.title')
                    ->setName('baseapp_cms_structure')
                    ->setRouteName('cms_structure')
                    ->setIcon('fa fa-font fa-sm fa-fw mr-2 text-gray-400'),
                1
            );
        }
        if (UserService::$instance->isAllowed('cms_media')) {
            $container->add(
                Level2MenuEntry::create()
                    ->setTitle('baseapp.cms.media.menu')
                    ->setName('baseapp_cms_media')
                    ->setRouteName('cms_media')
                    ->setIcon('fas fa-images fa-sm fa-fw mr-2 text-gray-400'),
                1
            );
        }
        if (AppService::$instance->isBackendMode()) {
            $headStyle = $guiBuilder->getHeadStyleSheets();
            $headStyle->add(InternStyleSheet::create()->setSource('/bundles/cms/css/cms.css'));
            $headStyle->add(InternStyleSheet::create()->setSource('/bundles/cms/css/codemirror.css'));
            $headJs = $guiBuilder->getHeadJavascripts();
            $headJs->add(InternJavascript::create()->setTitle('/bundles/cms/js/cms.js'));
            $headJs->add(InternJavascript::create()->setTitle('/bundles/cms/js/codemirror.js'));
            $headJs->add(InternJavascript::create()->setTitle('/bundles/cms/js/css.js'));
            $headJs->add(InternJavascript::create()->setTitle('/bundles/cms/js/javascript.js'));
        }

        $userMenu = $guiBuilder->getTopMenuRight()->findElementByName('baseapp_user_menu');
        $userMenu && $userMenu->getChildContainer()->removeElementByName('websocket');

        $topMenu = $guiBuilder->getTopMenuRight();
        $topMenu->add(Level1Divider::create()->setName('baseapp_top_divider'),28);
        $topMenu->add(Cache::create()->setName('cms_cache'),25);
        $topMenu->add(Publish::create()->setName('cms_publish'),20);
    }

    /**
     * @param GuiBuilder $guiBuilder
     */
    public function modify(GuiBuilder $guiBuilder): void
    {
    }
}
