<?php

namespace Shape\CmsBundle\Gui\Elements;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Alerts
 * @package BaseApp\BaseappBundle\Gui\Elements
 */
class Cache extends GuiView
{
    protected $template = '@Cms/cache/header.html.twig';
}