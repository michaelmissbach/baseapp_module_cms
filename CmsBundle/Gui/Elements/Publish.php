<?php

namespace Shape\CmsBundle\Gui\Elements;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Alerts
 * @package BaseApp\BaseappBundle\Gui\Elements
 */
class Publish extends GuiView
{
    protected $template = '@Cms/publish/header.html.twig';
}