<?php

namespace Shape\CmsBundle\Twig;

use BaseApp\BaseappBundle\Interfaces\ITree;
use Shape\CmsBundle\Event\Rendering\PostBodyContent;
use Shape\CmsBundle\Event\Rendering\PostHeadContent;
use Shape\CmsBundle\Event\Rendering\PreBodyContent;
use Shape\CmsBundle\Event\Rendering\PreHeadContent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class RendererExtension
 * @package BaseApp\BaseappBundle\Twig
 */
class CmsExtension extends AbstractExtension
{
    public function __construct(protected EventDispatcherInterface $dispatcher)
    {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('render_pre_head', [$this, 'renderPreHead'], ['is_safe' => ['html']]),
            new TwigFunction('render_post_head', [$this, 'renderPostHead'], ['is_safe' => ['html']]),
            new TwigFunction('render_pre_body', [$this, 'renderPreBody'], ['is_safe' => ['html']]),
            new TwigFunction('render_post_body', [$this, 'renderPostBody'], ['is_safe' => ['html']])
        ];
    }

    public function renderPreHead(ITree $page): string
    {
        $event = PreHeadContent::create($page);
        $this->dispatcher->dispatch($event, PreHeadContent::NAME);
        return $event->getContent();
    }

    public function renderPostHead(ITree $page): string
    {
        $event = PostHeadContent::create($page);
        $this->dispatcher->dispatch($event, PostHeadContent::NAME);
        return $event->getContent();
    }

    public function renderPreBody(ITree $page): string
    {
        $event = PreBodyContent::create($page);
        $this->dispatcher->dispatch($event, PreBodyContent::NAME);
        return $event->getContent();
    }

    public function renderPostBody(ITree $page): string
    {
        $event = PostBodyContent::create($page);
        $this->dispatcher->dispatch($event, PostBodyContent::NAME);
        return $event->getContent();
    }
}
