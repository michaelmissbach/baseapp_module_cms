<?php

namespace Shape\CmsBundle\Twig;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Event\Rendering\PostContent;
use BaseApp\BaseappBundle\Event\Rendering\PreContent;
use BaseApp\BaseappBundle\Interfaces\INode;
use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Service\AppService;
use BaseApp\BaseappBundle\Service\SettingsService;
use BaseApp\BaseappBundle\Service\StringHelper;
use BaseApp\BaseappBundle\Service\TreeService;
use BaseApp\BaseappBundle\Service\UserService;
use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\ContentMode;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Constant\RendererMode;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Interfaces\ITreeRenderable;
use Shape\CmsBundle\Node\Content\Container;
use Shape\CmsBundle\Node\Content\Referencetarget;
use Shape\CmsBundle\Node\Structure\Referencesource;
use Shape\CmsBundle\Service\Renderer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\OptionsResolver\OptionConfigurator;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RendererExtension
 * @package BaseApp\BaseappBundle\Twig
 */
class RendererExtension extends AbstractExtension
{
    public function __construct(protected Renderer $renderer,
                                protected TreeContainer $treeContainer,
                                protected EntityManagerInterface $doctrine,
                                protected Environment $twig)
    {}
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('render_container', [$this, 'renderContainer'], ['is_safe' => ['html']]),
            new TwigFunction('render_child', [$this, 'renderChilds'], ['is_safe' => ['html']]),
            new TwigFunction('render_node', [$this, 'renderNode'], ['is_safe' => ['html']]),
            new TwigFunction('render_content', [$this, 'renderContent'], ['is_safe' => ['html']]),
            new TwigFunction('render_edit_tags', [$this, 'renderEditTags'], ['is_safe' => ['html']]),
            new TwigFunction('render_reference_source', [$this, 'renderReferenceSource'], ['is_safe' => ['html']]),
        ];
    }

    public function renderContainer(ITreeRenderable $root, string $containerName): string
    {
        /** @var ITreeRenderable $child */
        foreach ($root->getChildren() as $container) {
            if ($container->getNodeType() !== Container::getName()) {
                throw new \Exception('Contenttype "Container" expected!');
            }

            $treeOptions = $container->getOptions();
            if (isset($treeOptions[OptionKeys::CONTAINER]) && strlen($treeOptions[OptionKeys::CONTAINER])) {
                if ($treeOptions[OptionKeys::CONTAINER] === $containerName) {
                    return $this->renderer->render($container);
                }
            }
        }
        if ($this->renderer->getMode() === RendererMode::EDIT) {
            return sprintf('Container [%s] not found.', $containerName);
        }
        return '';
    }

    public function renderChilds(ITreeRenderable $element): string
    {
        return $this->renderer->render($element);
    }

    public function renderContent(ITreeRenderable $element, string $subContent = 'content'): string
    {
        return $element->getContent()[$subContent] ?? '';
    }

    public function renderNode(ITreeRenderable $element): string
    {
        if (!$element->getOption(OptionKeys::HIDDEN, false)) {
            if ($node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('content', $element)) {
                return $this->renderer->renderNode($node);
            }
        }
        return '';
    }

    public function renderEditTags(ITreeRenderable $element, string $subContent = 'content')
    {
        $content = '';
        if ($this->renderer->getMode() === RendererMode::EDIT) {
            $node = $this->treeContainer->getNodeByTreeElementNameAndTreeElement('content', $element);
            return sprintf(
                ' data-cms-edit="true" data-cms-content-id="%s" data-cms-type="%s" data-cms-content-key="%s" %s ',
                $element->getId(),
                $element->getNodeType(),
                $subContent,
                $node->renderAdditionalEditTags()
            );
        }
        return $content;
    }

    public function renderReferenceSource(string $name): string
    {
        $entities =
            $this->doctrine->getRepository(Structure::class)
                ->findByNodeType(Referencesource::getName());
        foreach ($entities as $entity) {
            if ($entity->getOption(OptionKeys::TITLE) === $name) {
                if ($content = $this->doctrine->getRepository(Content::class)->findOneBy(['foreignId' => $entity->getId(), 'parent' => null, 'mode' => ContentMode::PUBLISH])) {
                    return Referencetarget::renderReferenceContent($this->renderer, $content);
                }
            }
        }
        return '';
    }
}
