<?php

namespace Shape\CmsBundle\Entity;

use BaseApp\BaseappBundle\Abstracts\AbstractTree;
use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Traits\CreatedAndModifiedTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: "cms_structure")]
class Structure extends AbstractTree implements ITree
{
    use CreatedAndModifiedTrait;

    #[ORM\ManyToOne(targetEntity: Structure::class, inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id')]
    protected ?ITree $parent;

    #[ORM\OneToMany(targetEntity: Structure::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['sorting' => 'ASC'])]
    protected Collection $children;

    #[ORM\Column(name: 'built_url', type: Types::STRING, length: 2000, nullable: true)]
    protected ?string $builtUrl;

    public static function getType(): string
    {
        return 'structure';
    }

    public static function create(): ITree
    {
        return new Structure();
    }

    public function getBuiltUrl(): ?string
    {
        return $this->builtUrl;
    }

    public function setBuiltUrl(?string $builtUrl): void
    {
        $this->builtUrl = $builtUrl;
    }
}
