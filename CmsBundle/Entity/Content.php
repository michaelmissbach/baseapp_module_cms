<?php

namespace Shape\CmsBundle\Entity;

use BaseApp\BaseappBundle\Abstracts\AbstractTree;
use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Traits\CreatedAndModifiedTrait;
use Shape\CmsBundle\Interfaces\ITreeRenderable;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: "cms_content")]
class Content extends AbstractTree implements ITree, ITreeRenderable
{
    use CreatedAndModifiedTrait;

    #[ORM\ManyToOne(targetEntity: Content::class, inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    protected ?ITree $parent;

    #[ORM\OneToMany(targetEntity: Content::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['sorting' => 'ASC'])]
    protected Collection $children;

    #[ORM\Column(name: 'mode', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $mode;

    public static function getType(): string
    {
        return 'content';
    }

    public static function create(): ITree
    {
        return new Content();
    }

    public function getMode(): ?string
    {
        return $this->mode;
    }

    public function setMode(?string $mode): void
    {
        $this->mode = $mode;
    }
}
