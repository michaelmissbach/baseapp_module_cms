<?php

namespace Shape\CmsBundle\Entity;

use BaseApp\BaseappBundle\Abstracts\AbstractTree;
use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Traits\CreatedAndModifiedTrait;
use Shape\CmsBundle\Interfaces\ITreeRenderable;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: "cms_media")]
class Media extends AbstractTree implements ITree, ITreeRenderable
{
    use CreatedAndModifiedTrait;

    #[ORM\ManyToOne(targetEntity: Media::class, inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id')]
    protected ?ITree $parent;

    #[ORM\OneToMany(targetEntity: Media::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['sorting' => 'ASC'])]
    protected Collection $children;

    #[ORM\Column(name: 'raw', type: Types::TEXT, nullable: true)]
    protected ?string $raw;

    #[ORM\Column(name: 'identity', type: Types::STRING, nullable: true)]
    protected ?string $identity;

    #[ORM\Column(name: 'extension', type: Types::STRING, nullable: true)]
    protected ?string $extension;

    public static function getType(): string
    {
        return 'media';
    }

    public static function create(): ITree
    {
        return new Media();
    }

    public function getRaw(): ?string
    {
        return $this->raw;
    }

    public function setRaw(?string $raw): void
    {
        $this->raw = $raw;
    }

    public function getIdentity(): ?string
    {
        return $this->identity;
    }

    public function setIdentity(?string $identity): void
    {
        $this->identity = $identity;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(?string $extension): void
    {
        $this->extension = $extension;
    }
}
