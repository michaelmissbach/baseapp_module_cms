<?php

namespace Shape\CmsBundle\Cache;

use BaseApp\BaseappBundle\Service\StringHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

class ContentCache
{
    public function __construct(protected KernelInterface $kernel) {}

    public static function getFolder(): string
    {
        return '/var/cache/page';
    }

    protected static function buildFileName($serverName, $path): string
    {
        return sprintf('%s.%s.cache', $serverName, str_replace('/','___.', $path));
    }

    public static function get(): void
    {
        $filePath = sprintf('..%s/', self::getFolder());
        $fileName = self::buildFileName($_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']);
        if (file_exists($filePath.$fileName)) {
            echo file_get_contents($filePath.$fileName);
            die;
        }
    }

    public function save(Request $request, $path, $content): void
    {
        if (!StringHelper::startsWith($path, '/')) {
            $path = sprintf('/%s', $path);
        }
        $filePath = sprintf('%s%s/', $this->kernel->getProjectDir(), self::getFolder());
        $fileName = self::buildFileName($request->server->get('SERVER_NAME'), $path);
        file_put_contents($filePath.$fileName, $content);
    }

    public function clear(): void
    {
        $files = glob(sprintf('%s/*',sprintf('%s%s/', $this->kernel->getProjectDir(), self::getFolder()))); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file)) {
                unlink($file); // delete file
            }
        }
    }
}
