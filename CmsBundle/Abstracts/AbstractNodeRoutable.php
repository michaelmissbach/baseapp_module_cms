<?php

namespace Shape\CmsBundle\Abstracts;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Interfaces\INodeRenderable;
use Shape\CmsBundle\Interfaces\INodeRoutable;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

abstract class AbstractNodeRoutable extends AbstractNode implements INodeRoutable
{
    public function optionsFormBuilder(FormBuilderInterface $formBuilder, ParameterBag $parameterBag): void
    {
        $formBuilder
            ->add(OptionKeys::HIDDEN, CheckboxType::class, [
                'label' => 'baseapp.cms.options.hidden'
            ])
            ->add(OptionKeys::SHOW_IN_MENUS, CheckboxType::class, [
                'label' => 'baseapp.cms.options.showinmenus'
            ])
            ->add(OptionKeys::USE_AS_URL_SEGMENT, CheckboxType::class, [
                'label' => 'baseapp.cms.options.useasurlsegment'
            ])
        ;
    }
}