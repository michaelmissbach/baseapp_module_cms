<?php

namespace Shape\CmsBundle\Abstracts;

use BaseApp\BaseappBundle\Abstracts\AbstractNode;
use BaseApp\BaseappBundle\Interfaces\ITree;
use Shape\CmsBundle\Event\Content\NodeTemplate;
use Shape\CmsBundle\Interfaces\INodeRenderable;
use Symfony\Component\HttpFoundation\ParameterBag;
use Twig\Environment;

abstract class AbstractNodeRenderable extends AbstractNode implements INodeRenderable
{
    public function render(Environment $twig, ParameterBag $parameters): string
    {
        return '';
    }

    protected function buildTemplatePath(ITree $page, string $template): string
    {
        $nodeTemplate = NodeTemplate::create($page, $template);
        $this->context->get('dispatcher')->dispatch($nodeTemplate, NodeTemplate::NAME);
        return $nodeTemplate->getBuiltTemplatePath();
    }

    public function renderAdditionalEditTags(): string
    {
        return '';
    }

    public function canInsertPreAndPostContent(): bool
    {
        return true;
    }

    public function isCacheble(): bool
    {
        return true;
    }
}