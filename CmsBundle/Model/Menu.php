<?php

namespace Shape\CmsBundle\Model;

class Menu
{
    protected int $id;

    protected ?string $url;

    protected string $title;

    protected ?Menu $parent;

    protected array $children = [];

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getParent(): ?Menu
    {
        return $this->parent;
    }

    public function setParent(?Menu $parent): void
    {
        $this->parent = $parent;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function addChild(Menu $child): void
    {
        $this->children[] = $child;
    }

    public function setChildren(array $children): void
    {
        $this->children = $children;
    }
}