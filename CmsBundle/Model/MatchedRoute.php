<?php

namespace Shape\CmsBundle\Model;

use BaseApp\BaseappBundle\Interfaces\ITree;

class MatchedRoute
{
    protected ?Itree $matchedPage = null;
    protected ?ITree $forwardToPage = null;
    protected ?ITree $redirectToPage = null;

    public function getMatchedPage(): ?Itree
    {
        return $this->matchedPage;
    }

    public function setMatchedPage(?Itree $matchedPage): void
    {
        $this->matchedPage = $matchedPage;
    }

    public function getRedirectToPage(): ?ITree
    {
        return $this->redirectToPage;
    }

    public function setRForwardToPage(?ITree $forwardToPage): void
    {
        $this->forwardToPage = $forwardToPage;
    }

    public function setRedirectToPage(?ITree $redirectToPage): void
    {
        $this->redirectToPage = $redirectToPage;
    }
}
