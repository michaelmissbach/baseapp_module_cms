<?php

namespace Shape\CmsBundle\EventSubscriber;

use Shape\CmsBundle\Event\Rendering\ErrorPage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Environment;

class ErrorPageSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected Environment $twig
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            ErrorPage::NAME => ['errorPage',0]
        ];
    }

    public function errorPage(ErrorPage $event): void
    {
        $content = $this->twig->render(
            '@Cms/errorpage/index.html.twig',
            [
                'message' => $event->getMessage()
            ]
        );
        $event->setRenderedPage($content);
    }
}