<?php

namespace Shape\CmsBundle\EventSubscriber;

use BaseApp\BaseappBundle\Tree\TreeContainer;
use Shape\CmsBundle\Constant\RendererMode;
use Shape\CmsBundle\Event\Rendering\PostBodyContent;
use Shape\CmsBundle\Event\Rendering\PreHeadContent;
use Shape\CmsBundle\Interfaces\INodeEditAssetsIncludeable;
use Shape\CmsBundle\Service\Renderer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InPlaceSubscriber implements EventSubscriberInterface
{
    public function __construct(protected Renderer $renderer, protected TreeContainer $treeContainer) {}

    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            PreHeadContent::NAME => ['preHeadContent',0],
            PostBodyContent::NAME => ['postBodyContent',0],

        ];
    }

    public function preHeadContent(PreHeadContent $event): void
    {
        if ($this->renderer->getMode() === RendererMode::EDIT) {
            $event->addContent('<meta http-equiv=”Pragma” content=”no-cache”><meta http-equiv=”Expires” content=”-1″><meta http-equiv=”CACHE-CONTROL” content=”NO-CACHE”>');
        }
    }

    public function postBodyContent(PostBodyContent $event): void
    {
        if ($this->renderer->getMode() === RendererMode::EDIT) {
            $event->addContent('<link rel="stylesheet" type="text/css" href="/bundles/cms/css/in_place.css"></link>');
            $event->addContent('<script src="/bundles/cms/js/ckeditor-inline.js"></script>');
            $event->addContent('<script src="/bundles/baseapp/js/communicate.js"></script>');
            $event->addContent('<script src="/bundles/cms/js/in_place.js"></script>');

            foreach ($this->treeContainer->getNodesByTreeElementName('content') as $instance) {
                if (is_subclass_of($instance, INodeEditAssetsIncludeable::class)) {
                    $instance::addInPlaceAssets($event);
                }
            }
        }
    }
}
