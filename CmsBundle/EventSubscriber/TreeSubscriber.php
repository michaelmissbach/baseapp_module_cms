<?php

namespace Shape\CmsBundle\EventSubscriber;

use BaseApp\BaseappBundle\Event\Tree\AccessLoad;
use BaseApp\BaseappBundle\Event\Tree\NodeAllowedCategories;
use BaseApp\BaseappBundle\Event\Tree\NodeContextBuild;
use BaseApp\BaseappBundle\Event\Tree\OptionsContext;
use BaseApp\BaseappBundle\Event\Tree\PostElementAdd;
use BaseApp\BaseappBundle\Event\Tree\PostElementCopy;
use BaseApp\BaseappBundle\Event\Tree\PreElementRemove;
use BaseApp\BaseappBundle\Event\Tree\TypeFilter;
use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Service\TreeService;
use Shape\CmsBundle\Constant\ContentMode;
use Shape\CmsBundle\Constant\ContentType;
use Shape\CmsBundle\Constant\OptionKeys;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Node\Content\Container;
use Shape\CmsBundle\Node\Content\Grid;
use Shape\CmsBundle\Node\Content\Grid1;
use Shape\CmsBundle\Node\Content\Grid2;
use Shape\CmsBundle\Node\Content\Grid3;
use Shape\CmsBundle\Node\Content\Grid4;
use Shape\CmsBundle\Node\Content\SeoContent;
use Shape\CmsBundle\Node\Content\SiteContent;
use Shape\CmsBundle\Node\Structure\Error;
use Shape\CmsBundle\Node\Structure\Page;
use Shape\CmsBundle\Node\Content\Root;
use Shape\CmsBundle\Node\Structure\Referencesource;
use Shape\CmsBundle\Service\MenuService;
use Shape\CmsBundle\Service\Renderer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TreeSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected EntityManagerInterface $doctrine,
        protected TreeService $treeService,
        protected ParameterBagInterface $parameterBag,
        protected MenuService $menuService,
        protected Renderer $renderer,
        protected EventDispatcherInterface $dispatcher,
        protected TranslatorInterface $translator
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            PostElementAdd::NAME => ['postElementAdd',0],
            PreElementRemove::NAME => ['preElementRemove',0],
            OptionsContext::NAME => ['optionsContext', 0],
            NodeContextBuild::NAME => ['nodeContext',0],
            AccessLoad::NAME => ['accessLoad', 0],
            TypeFilter::NAME => ['typeFilter', 0],
            PostElementCopy::NAME => ['postElementCopy',0],
            NodeAllowedCategories::NAME => ['nodeAllowedCategories', -50]
        ];
    }

    public function preElementRemove(PreElementRemove $event): void
    {
        if ($event->getTreeElement() instanceof Structure) {
            $elements = $this->doctrine->getRepository(Content::class)
                ->findBy(['foreignId' => $event->getTreeElement()->getId(), 'parent' => null]);
            /** @var ITree $element */
            foreach ($elements as $element) {
                $this->treeService->removeNode('content', $element->getId(), true, true, true);
            }
        }
    }

    public function postElementAdd(PostElementAdd $event): void
    {
        if ($event->getTreeElement() instanceof Structure) {
            if ($event->getTreeElement()->getNodeType() === Page::getName()
                || $event->getTreeElement()->getNodeType() === Referencesource::getName()
                || $event->getTreeElement()->getNodeType() === Error::getName()
            ) {

                $event->getTreeElement()->addOption(OptionKeys::HIDDEN, true);
                $this->doctrine->persist($event->getTreeElement());

                if ($node = $this->treeService->addNode(
                    'content',
                    Root::getName(),
                    null,
                    1,
                    $event->getTreeElement()->getId()
                    )) {
                    if ($event->getTreeElement()->getNodeType() === Page::getName()
                        || $event->getTreeElement()->getNodeType() === Error::getName()
                    ) {
                        $this->treeService->addNode(
                            'content',
                            SeoContent::getName(),
                            $node->getId(),
                            1,
                            $event->getTreeElement()->getId()
                        );
                        $siteContent = $this->treeService->addNode(
                            'content',
                            SiteContent::getName(),
                            $node->getId(),
                            1,
                            $event->getTreeElement()->getId()
                        );
                        if ($config = $this->renderer->getTemplateConfigFromITree($event->getTreeElement())) {
                            //that can not happen, because the page template is setted when page editied in backend!
                            foreach ($config['containers'] as $containerName) {
                                $containerElement = $this->treeService->addNode(
                                    'content',
                                    Container::getName(),
                                    $siteContent->getId(),
                                    1,
                                    $event->getTreeElement()->getId()
                                );
                                $containerElement->addOption(OptionKeys::CONTAINER, $containerName);
                                $this->doctrine->persist($containerElement);
                            }
                        }
                    } else if ($event->getTreeElement()->getNodeType() === Referencesource::getName()) {
                        $event->getTreeElement()->addOption(OptionKeys::TEMPLATE, '@Cms/structure/referencesource.html.twig');
                        $this->treeService->addNode(
                            'content',
                            SiteContent::getName(),
                            $node->getId(),
                            1,
                            $event->getTreeElement()->getId()
                        );
                        $this->doctrine->persist($event->getTreeElement());
                    }
                }
                $this->doctrine->flush();
            }

        }
        if ($event->getTreeElement() instanceof Content) {
            if ($event->getTreeElement()->getNodeType() === Root::getName()) {
                $event->getTreeElement()->setMode(ContentMode::DRAFT);
                $this->doctrine->persist($event->getTreeElement());
                $this->doctrine->flush();
            }

            if ($event->getTreeElement()->getNodeType() === Grid1::getName() ||
                $event->getTreeElement()->getNodeType() === Grid2::getName() ||
                $event->getTreeElement()->getNodeType() === Grid3::getName() ||
                $event->getTreeElement()->getNodeType() === Grid4::getName()
            ) {
                $counter = (int)str_replace('grid', '',$event->getTreeElement()->getNodeType());
                $percent = (int)(100 / $counter);
                for($i = 1; $i <= $counter; $i++) {
                    $node = $this->treeService->addNode(
                        'content',
                        Grid::getName(),
                        $event->getTreeElement()->getId(),
                        $event->getTreeElement()->getVersion(),
                        $event->getTreeElement()->getForeignId()
                    );
                    $node->addOption('percent', $percent);
                    $this->doctrine->persist($node);
                }
                $this->doctrine->flush();
            }
        }
    }

    public function optionsContext(OptionsContext $event): void
    {
        if ($event->getTreeElement() instanceof Structure || $event->getTreeElement() instanceof Content) {
            $bag = $event->getParameterBag();
            $bag->set('config', $this->parameterBag->get('cms'));
        }
    }

    public function nodeContext(NodeContextBuild $event): void
    {
        $event->getContext()->set('config', $this->parameterBag->get('cms'));
        $event->getContext()->set('menuService', $this->menuService);
        $event->getContext()->set('renderer', $this->renderer);
        $event->getContext()->set('dispatcher', $this->dispatcher);
    }

    public function accessLoad(AccessLoad $event): void
    {
        $params = $event->getParams();
        if ($params['name'] === 'content') {
            $entity = $this->doctrine->getRepository(Content::class)
                ->findOneBy(['parent'=>null, 'version' => $params['version'], 'foreignId' => $params['foreignId']]);
            if ($entity && $entity->getMode() !== ContentMode::DRAFT) {
                $event->setIsAllowed(false);
            }
        }
    }

    public function typeFilter(TypeFilter $event): void
    {
        if ($event->getTreeName() === 'media' && $event->getType()) {
            if ($event->getNode()::getName() === \Shape\CmsBundle\Node\Media\Root::getName() ||
                $event->getNode()::getName() === \Shape\CmsBundle\Node\Media\Folder::getName()
            ) {
                return;
            }
            if ($event->getNode()::getName() !== $event->getType()) {
                $event->setIsAllowed(false);
            }
        }

        if ($event->getTreeName() === 'content') {
            if ($event->getNode()::getName() === SeoContent::getName()) {
                $event->setIsAllowed(false);
            }
        }
    }

    public function postElementCopy(PostElementCopy $event): void
    {
        if ($event->getTarget() instanceof Structure) {
            $target = $event->getTarget();
            if ($title = $target->getOption(OptionKeys::TITLE)) {
                $title = sprintf('%s %s', $this->translator->trans('baseapp.tree.structure.copyof'), $title);
            } else {
                $title = $this->translator->trans('baseapp.tree.structure.copyof');
            }
            $target->addOption(OptionKeys::TITLE, $title);
            $target->addOption(OptionKeys::HIDDEN, true);
            $this->doctrine->persist($target);
            $this->doctrine->flush();

            $contentList = $this->doctrine->getRepository(Content::class)
                ->findBy(['parent'=>null, 'foreignId'=>$event->getSource()->getId()]);
            foreach($contentList as $content) {
                $this->treeService->copyTreeElement($content, $content->getVersion(), $target->getId());
            }
        }
    }

    public function nodeAllowedCategories(NodeAllowedCategories $event): void
    {
        if ($event->getTreeName() === 'content') {
            if ($structure = $this->doctrine->getRepository(Structure::class)->find($event->getTreeElement()->getForeignId())) {
                if ($structure->getNodeType() === Referencesource::getName()) {
                    if ($event->getNode()::getName() === SiteContent::getName()) {
                        $event->setCategories([
                            sprintf('baseapp.content.node.category.%s',ContentType::GRID),
                            sprintf('baseapp.content.node.category.%s',ContentType::CONTENT)
                        ]);
                    }
                }
            }
        }
    }
}
