<?php

namespace Shape\CmsBundle\EventSubscriber;

use BaseApp\BaseappBundle\Event\Tree\AccessLoad;
use BaseApp\BaseappBundle\Event\Tree\OptionsContext;
use BaseApp\BaseappBundle\Event\Tree\PostElementAdd;
use BaseApp\BaseappBundle\Event\Tree\PreElementRemove;
use BaseApp\BaseappBundle\Interfaces\ITree;
use BaseApp\BaseappBundle\Service\TreeService;
use Shape\CmsBundle\Constant\ContentMode;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Event\Media\Uploaded;
use Shape\CmsBundle\Node\Media\Image;
use Shape\CmsBundle\Node\Structure\Page;
use Shape\CmsBundle\Node\Content\Root;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MediaSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected EntityManagerInterface $doctrine,
        protected TreeService $treeService,
        protected ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            Uploaded::NAME => ['uploaded', 0]
        ];
    }

    public function uploaded(Uploaded $event): void
    {
        $extension = strtolower($event->getExtension());
        if (in_array($extension, ['jpg','png'])) {
            $uploadedFile = $event->getUploadedFile();
            $event->setOptions([
                'title' => '',
                'alt' => '',
                'originalName' => $uploadedFile->getClientOriginalName(),
                'originalExtension' => $uploadedFile->getClientOriginalExtension(),
                'mimeType' => $uploadedFile->getClientMimeType()
            ]);
            $event->setRaw(base64_encode(file_get_contents($uploadedFile->getPathname())));
            $event->setMatchedNodename(Image::getName());
            $event->setProcessed(true);
        }
    }
}
