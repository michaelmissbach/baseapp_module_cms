<?php

namespace Shape\CmsBundle\EventSubscriber;

use Shape\CmsBundle\Event\Content\NodeTemplate;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FallbackSubscriber implements EventSubscriberInterface
{
    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            NodeTemplate::NAME => ['nodeTemplate',-255]
        ];
    }

    public function nodeTemplate(NodeTemplate $event): void
    {
        if (!$event->getBuiltTemplatePath()) {
            $event->setBuiltTemplatePath(sprintf(
                '@Cms/content/bootstrap/%s',
                $event->getTemplate()
            ));
            $event->stopPropagation();
        }
    }
}