<?php

namespace Shape\CmsBundle\EventSubscriber;

use Shape\CmsBundle\Cache\ContentCache;
use Shape\CmsBundle\Event\Cache\Clear;
use Shape\CmsBundle\Service\MenuService;
use Shape\CmsBundle\Service\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CacheSubscriber implements EventSubscriberInterface
{
    public function __construct(protected ContentCache $contentCache,
                                protected Router $router,
                                protected TranslatorInterface $translator,
                                protected MenuService $menu
    ) {}

    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            Clear::NAME => ['clear', 0]
        ];
    }

    public function clear(Clear $event): void
    {
        $this->router->buildTree();
        $event->addMessage($this->translator->trans('baseapp.cms.structure.routes.built'));
        $this->contentCache->clear();
        $event->addMessage($this->translator->trans('baseapp.cms.structure.cache.cleared'));
        $this->menu->buildTree();
        $event->addMessage($this->translator->trans('baseapp.cms.structure.menu.built'));
    }
}
