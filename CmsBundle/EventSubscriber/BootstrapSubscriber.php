<?php

namespace Shape\CmsBundle\EventSubscriber;

use BaseApp\BaseappBundle\Event\Tree\OptionsFormBuilder;
use BaseApp\BaseappBundle\Interfaces\ITree;
use Shape\CmsBundle\Entity\Content;
use Shape\CmsBundle\Entity\Structure;
use Shape\CmsBundle\Event\Content\NodeTemplate;
use Shape\CmsBundle\Node\Content\Grid;
use Shape\CmsBundle\Node\Structure\Page;
use Shape\CmsBundle\Service\Renderer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BootstrapSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected EntityManagerInterface $doctrine,
        protected Renderer $renderer
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents()
    {
        return [
            NodeTemplate::NAME => ['nodeTemplate',0],
            OptionsFormBuilder::NAME => ['optionsFormBuilder', 0]
        ];
    }

    protected function isBootstrap(ITree $page): bool
    {
        if($config = $this->renderer->getTemplateConfigFromITree($page)) {
            if (isset($config['type'])) {
                return $config['type'] === 'bootstrap';
            }
        }
        return false;
    }

    public function nodeTemplate(NodeTemplate $event): void
    {
        if ($this->isBootstrap($event->getPage())) {
            $event->setBuiltTemplatePath(sprintf(
                '@Cms/content/bootstrap/%s',
                $event->getTemplate()
            ));
            $event->stopPropagation();
        }
    }

    public function optionsFormBuilder(OptionsFormBuilder $event): void
    {
        if ($event->getTreeElement() instanceof Content) {
            if ($event->getTreeElement()->getNodeType() === Grid::getName()) {
                if ($page = $this->doctrine->getRepository(Structure::class)->find(($event->getTreeElement()->getForeignId()))) {
                    if ($this->isBootstrap($page)) {
                        $event->getFormBuilder()
                            ->add('percent', ChoiceType::class, [
                                'data' => $event->getTreeElement()->getOption('percent'),
                                'choices' => [
                                    '8%' => '8',
                                    '16%' => '16',
                                    '25%' => '25',
                                    '33%' => '33',
                                    '41%' => '41',
                                    '50%' => '50',
                                    '58%' => '58',
                                    '66%' => '66',
                                    '75%' => '75',
                                    '83%' => '83',
                                    '91%' => '91',
                                    '100%' => '100'
                                ]
                            ])
                        ;
                    }
                }
            }
        }
        if ($event->getTreeElement() instanceof Structure) {
            if ($event->getTreeElement()->getNodeType() === Page::getName()) {
                if ($this->isBootstrap($event->getTreeElement())) {
                    $event->getFormBuilder()
                        ->add('layout-step', ChoiceType::class, [
                            'data' => $event->getTreeElement()->getOption('layout-step'),
                            'placeholder' => 'baseapp.common.pleasechoose',
                            'choices' => [
                                'XS'  => 'xs-',
                                'SM'  => 'sm-',
                                'MD'  => 'md-',
                                'LG'  => 'lg-',
                                'XL'  => 'xl-',
                                'XXL'  => 'xxl-',
                            ]
                        ])
                    ;
                }
            }
        }
    }
}